package com.gmi.littlesquare.constant;

/**
 * Created by pablo on 15/09/14.
 */
public enum SquareDifficulty {
    FACIL, RELATIVAMENTE_FACIL, MEDIANO, DIFICIL, MUY_DIFICIL, DIFICILISIMO
}
