package com.gmi.littlesquare.logic;

import com.gmi.littlesquare.GameActivity;
import com.gmi.littlesquare.constant.SquareDifficulty;
import com.gmi.littlesquare.domain.AbstractSquare;
import com.gmi.littlesquare.domain.EnemySquare;
import com.gmi.littlesquare.domain.FriendSquare;
import com.gmi.littlesquare.domain.LifeSquare;
import com.gmi.littlesquare.domain.PlayerSquare;
import com.gmi.littlesquare.domain.PowerSquare;

import org.andengine.entity.IEntity;
import org.andengine.entity.IEntityMatcher;
import org.andengine.entity.modifier.CubicBezierCurveMoveModifier;
import org.andengine.entity.scene.Scene;

/**
 * Created by pablo on 15/09/14.
 */
public class SquareLogic {

    private GameActivity gameActivity;

    /**
     * Tiempo que debe de
     * pasar para que salga
     * un nuevo cuadrado.
     * Este tiempo va cambiando
     * a lo largo del juego
     */
    private long TIEMPO_SALIDA_CUADRADO = 1500;

    /**
     * Milisegundos que pasaron
     * desde que se libero el
     * ultimo cuadrado
     */
    private long salidaUltimoCuadrado;

    /**
     * Cantidad de cuadrados que salieron
     */
    public int cantidadCuadradosSalidos = 0;
    public int cantidadCuadradosAmigosSalidos;
    public int cantidadCuadradosEnemigosSalidos;

    /**
     * El siguiente array define en que salida
     * de cuadrados sale cada nuevo cuadrado
     * frenetico
     */
    private int[] numerosCuadradosSalidosAparicionFrenetico = {40, 100, 150, 200, 300, 400, 500, 600, 700, 800, 900, 1000};

    /**
     * El siguiente array define en que salida
     * de cuadrados sale cada nuevo cuadrado
     * de vida
     */
    private int[] numerosCuadradosSalidosAparicionVida = {2, 10, 15, 20, 25, 30, 35, 40, 45, 50};


    public SquareLogic(GameActivity gameActivity) {
        this.gameActivity = gameActivity;
    }

    public void update(float ms) {

        //ya paso el tiempo necesario para
        //que se libere un nuevo cuadrado
        if(salidaUltimoCuadrado <= 0) {

            //configuramos el nuevo tiempo
            //de salida de cuadrados
            salidaUltimoCuadrado = TIEMPO_SALIDA_CUADRADO;

            //liberamos un nuevo cuadrado
            releaseSquare();

        }
        else {
            //decrementamos el tiempo de
            //salida del proximo cuadrado
            salidaUltimoCuadrado -= ms;
        }

        //verify player square collisions
        verifyPlayerSquareCollision();


    }

    private void verifyPlayerSquareCollision() {
        Scene scene = gameActivity.getScene();
        PlayerSquare playerSquare = gameActivity.getPlayerSquare();
        int sceneChilds = scene.getChildCount();
        for(int i=0; i<sceneChilds; i++) {
            IEntity entity = scene.getChildByIndex(i);
            if(entity.getUserData() == AbstractSquare.ABSTRACT_SQUARE_ID) {
                AbstractSquare abstractSquare = (AbstractSquare) entity;
                if(playerSquare.collidesWith(abstractSquare)) {
                    abstractSquare.collides(playerSquare);
                    playerSquare.collides(abstractSquare);
                }
            }
        }
    }

    /**
     * Libera un cuadrado amigo o enemigo
     * y configura la dificultad en base
     * a la cantidad de cuadrados que ya
     * salieron
     */
    private void releaseSquare() {

        //calculamos en base a cuantos
        //cuadrados van saliendo la
        //probabilidad de que salga un
        //cuadrado amigo
        int probabilidadSalidaAmigo = 100;

        //difficulty asignada
        SquareDifficulty difficulty = null;

        //en base a los que salieron evaluamos
        //difficulty y probabilidad de salida
        //de cuadrado amigo
        if(cantidadCuadradosSalidos <= 10) {
            difficulty = SquareDifficulty.FACIL;
            TIEMPO_SALIDA_CUADRADO = 500;
        }
        else if(cantidadCuadradosSalidos > 10 && cantidadCuadradosSalidos <= 40) {
            probabilidadSalidaAmigo = 80;
            difficulty = SquareDifficulty.FACIL;
            TIEMPO_SALIDA_CUADRADO = 500;
        }
        else if(cantidadCuadradosSalidos > 40 && cantidadCuadradosSalidos <= 70) {
            probabilidadSalidaAmigo = 70;
            difficulty = SquareDifficulty.MEDIANO;
            TIEMPO_SALIDA_CUADRADO = 500;
        }
        else if(cantidadCuadradosSalidos > 70 && cantidadCuadradosSalidos <= 120) {
            probabilidadSalidaAmigo = 60;
            difficulty = SquareDifficulty.MEDIANO;
            TIEMPO_SALIDA_CUADRADO = 400;
        }
        else if(cantidadCuadradosSalidos > 120 && cantidadCuadradosSalidos <= 180) {
            probabilidadSalidaAmigo = 60;
            difficulty = SquareDifficulty.MEDIANO;
            TIEMPO_SALIDA_CUADRADO = 300;
        }
        else if(cantidadCuadradosSalidos > 180 && cantidadCuadradosSalidos <= 250) {
            probabilidadSalidaAmigo = 50;
            difficulty = SquareDifficulty.DIFICIL;
            TIEMPO_SALIDA_CUADRADO = 300;
        }
        else if(cantidadCuadradosSalidos > 250 && cantidadCuadradosSalidos <= 300) {
            probabilidadSalidaAmigo = 50;
            difficulty = SquareDifficulty.MUY_DIFICIL;
            TIEMPO_SALIDA_CUADRADO = 300;
        }
        else if(cantidadCuadradosSalidos > 300) {
            probabilidadSalidaAmigo = 50;
            difficulty = SquareDifficulty.DIFICILISIMO;
            TIEMPO_SALIDA_CUADRADO = 180;
        }

        //Flag que indica si sale
        //amigo o enemigo
        boolean saleAmigo = false;

        //En base a la probabilidad de
        //salida de cuadrados amigos
        //creamos un numero aleatorio
        float probabilidadEfectiva = (float)Math.random() * 100f;
        if(probabilidadEfectiva < probabilidadSalidaAmigo) {
            saleAmigo = true;
        }

        //referencia al cuadrado que va a salir
        AbstractSquare cuadradoSalidor = null;
        if(saleAmigo) {
            cuadradoSalidor = new FriendSquare(gameActivity, difficulty);
            cantidadCuadradosAmigosSalidos++;
        }
        else {
            cuadradoSalidor = new EnemySquare(gameActivity, difficulty);
            cantidadCuadradosEnemigosSalidos++;
        }

        //add square to scene
        gameActivity.getScene().attachChild(cuadradoSalidor);

        //incrementamos en uno la
        //cantidad de cuadrados salidos
        cantidadCuadradosSalidos++;

        //generacion de cuadrados de vida
        for(int numeroActual : numerosCuadradosSalidosAparicionVida) {

            //si el numero de salida de cuadrados
            //actual coincide con la lista de salida
            //de cuadrados de vida, entonces liberamos uno
            if(cantidadCuadradosSalidos == numeroActual) {
                //si salieron la cantidad de
                //cuadrados indicados entonces
                //generamos un nuevo cuadrado
                //de vida
                gameActivity.getScene().attachChild(new LifeSquare(gameActivity, difficulty));
            }
        }

        //generacion de cuadrados freneticos
        for(int numeroActual : numerosCuadradosSalidosAparicionFrenetico) {
            //si el numero de salida de cuadrados
            //actual coincide con la lista de salida
            //de cuadrados freneticos, entonces
            //liberamos uno
            if(cantidadCuadradosSalidos == numeroActual) {
                //si salieron la cantidad de
                //cuadrados indicados entonces
                //generamos un nuevo cuadrado
                //frenetico
                gameActivity.getScene().attachChild(new PowerSquare(gameActivity, difficulty));
            }
        }
    }
}