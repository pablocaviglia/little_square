package com.gmi.littlesquare;

import android.util.DisplayMetrics;
import android.view.MotionEvent;

import com.gmi.littlesquare.domain.GameHUD;
import com.gmi.littlesquare.domain.PlayerSquare;
import com.gmi.littlesquare.logic.SquareLogic;

import org.andengine.engine.camera.SmoothCamera;
import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.CroppedResolutionPolicy;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.IEntityModifier;
import org.andengine.entity.modifier.MoveYModifier;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.scene.background.ParallaxBackground;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.util.FPSLogger;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.view.RenderSurfaceView;
import org.andengine.ui.activity.SimpleBaseGameActivity;
import org.andengine.util.modifier.IModifier;

public class GameActivity extends SimpleBaseGameActivity implements IUpdateHandler, ITimerCallback {

    public static float CAMERA_WIDTH;
    public static float CAMERA_HEIGHT;
    private static final float CAMERA_FULL_ZOOM = 0.15f;

    public Scene scene;
    private SmoothCamera mSmoothCamera;

    private float[] previousTouchMoveCoords = new float[2];
    private float[] touchMoveDiff = new float[2];

    //player square
    private BitmapTextureAtlas squarePlayerTextureAtlas;
    private ITextureRegion squarePlayerRegion;
    private PlayerSquare playerSquare;

    //enemy square
    private BitmapTextureAtlas squareEnemyTextureAtlas;
    private ITextureRegion squareEnemyRegion;

    //friend square
    private BitmapTextureAtlas squareFriendTextureAtlas;
    private ITextureRegion squareFriendRegion;

    //life square
    private BitmapTextureAtlas squareLifeTextureAtlas;
    private ITextureRegion squareLifeRegion;

    //power square
    private BitmapTextureAtlas squarePowerTextureAtlas;
    private ITextureRegion squarePowerRegion;

    //pencil background
    private BitmapTextureAtlas backgroundTextureAtlas;
    private ITextureRegion backgroundRegion;

    //life left
    private BitmapTextureAtlas lifeLeftTextureAtlas;
    private ITextureRegion lifeLeftRegion;

    //life right
    private BitmapTextureAtlas lifeRightTextureAtlas;
    private ITextureRegion lifeRightRegion;

    //square release logic
    private SquareLogic squareLogic;

    //timers
    private TimerHandler powerTimer;
    public final int POWER_TIME = 10000;

    //hud
    private GameHUD hud;

    //lives
    private final static int INITIAL_LIVES = 10;
    private int lifes;

    @Override
    public EngineOptions onCreateEngineOptions() {

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        CAMERA_WIDTH = dm.widthPixels;
        CAMERA_HEIGHT = dm.heightPixels;

        this.mSmoothCamera = new SmoothCamera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT, 10, 10, 1.0f);
        return new EngineOptions(true, ScreenOrientation.PORTRAIT_SENSOR, new CroppedResolutionPolicy(CAMERA_WIDTH, CAMERA_HEIGHT), this.mSmoothCamera);
    }

    @Override
    protected void onSetContentView() {
        mRenderSurfaceView = new RenderSurfaceView(this);
        mRenderSurfaceView.setRenderer(mEngine, this);
        setContentView(mRenderSurfaceView, SimpleBaseGameActivity.createSurfaceViewLayoutParams());
    }

    @Override
    public void onCreateResources() {
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");

        //pencil background
        this.backgroundTextureAtlas = new BitmapTextureAtlas(this.getTextureManager(), 460, 1360, TextureOptions.BILINEAR);
        this.backgroundRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.backgroundTextureAtlas, this, "background_sky.jpg", 0, 0);
        this.backgroundTextureAtlas.load();

        //square player
        this.squarePlayerTextureAtlas = new BitmapTextureAtlas(getTextureManager(), 256, 256, TextureOptions.BILINEAR);
        this.squarePlayerRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.squarePlayerTextureAtlas, this, "square_player.png", 0, 0);
        this.squarePlayerTextureAtlas.load();

        //square enemy
        this.squareEnemyTextureAtlas = new BitmapTextureAtlas(getTextureManager(), 256, 256, TextureOptions.BILINEAR);
        this.setSquareEnemyRegion(BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.squareEnemyTextureAtlas, this, "square_enemy.jpg", 0, 0));
        this.squareEnemyTextureAtlas.load();

        //square friend
        this.squareFriendTextureAtlas = new BitmapTextureAtlas(getTextureManager(), 256, 256, TextureOptions.BILINEAR);
        this.setSquareFriendRegion(BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.squareFriendTextureAtlas, this, "square_friend.jpg", 0, 0));
        this.squareFriendTextureAtlas.load();

        //square life
        this.squareLifeTextureAtlas = new BitmapTextureAtlas(getTextureManager(), 256, 256, TextureOptions.BILINEAR);
        this.setSquareLifeRegion(BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.squareLifeTextureAtlas, this, "square_life.png", 0, 0));
        this.squareLifeTextureAtlas.load();

        //square power
        this.squarePowerTextureAtlas = new BitmapTextureAtlas(getTextureManager(), 256, 256, TextureOptions.BILINEAR);
        this.setSquarePowerRegion(BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.squarePowerTextureAtlas, this, "square_power.png", 0, 0));
        this.squarePowerTextureAtlas.load();

        //life left
        this.lifeLeftTextureAtlas = new BitmapTextureAtlas(getTextureManager(), 128, 256, TextureOptions.BILINEAR);
        this.setLifeLeftRegion(BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.lifeLeftTextureAtlas, this, "life_left.png", 0, 0));
        this.lifeLeftTextureAtlas.load();

        //life right
        this.lifeRightTextureAtlas = new BitmapTextureAtlas(getTextureManager(), 128, 256, TextureOptions.BILINEAR);
        this.setLifeRightRegion(BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.lifeRightTextureAtlas, this, "life_right.png", 0, 0));
        this.lifeRightTextureAtlas.load();

        //timers initialization
        powerTimer = new TimerHandler(10000, this);

    }

    @Override
    public Scene onCreateScene() {

        this.mEngine.registerUpdateHandler(new FPSLogger());

        //create scene
        scene = new Scene();
        scene.setTouchAreaBindingOnActionDownEnabled(true);
        scene.setBackground(new Background(0.09804f, 0.5274f, 0.1784f));

        //squares logic
        squareLogic = new SquareLogic(this);

        //create and animate background
        animateBackground();

        //player square
        playerSquare = new PlayerSquare(this, squarePlayerRegion);
        scene.attachChild(playerSquare);

        //register the update handler
        scene.registerUpdateHandler(this);

        //zoom
        mSmoothCamera.setZoomFactorDirect(CAMERA_FULL_ZOOM);

        scene.setOnSceneTouchListener(new IOnSceneTouchListener() {
            @Override
            public boolean onSceneTouchEvent(final Scene pScene, final TouchEvent pSceneTouchEvent) {

                MotionEvent motionEvent = pSceneTouchEvent.getMotionEvent();
                if(motionEvent.getPointerId(motionEvent.getActionIndex()) == 0) {
                    switch(pSceneTouchEvent.getAction()) {
                        case TouchEvent.ACTION_MOVE:

                            //always calculate the move difference
                            calculateTouchMoveDiff(pSceneTouchEvent);

                            //move player square
                            playerSquare.move(touchMoveDiff[0], touchMoveDiff[1]);

                            break;
                        case TouchEvent.ACTION_DOWN:
                            //reset the move difference
                            resetMoveDiff();

                            break;
                        case TouchEvent.ACTION_UP:
                            //reset the move difference
                            resetMoveDiff();

                            break;
                    }
                }
                return true;
            }
        });

        //set the hud
        hud = new GameHUD(this);
        mSmoothCamera.setHUD(hud);

        //add lifes
        lifes = INITIAL_LIVES;
        hud.addLifes(lifes, 2f);

        return scene;
    }

    private void animateBackground() {

        float BACKGROUND_WIDTH = CAMERA_WIDTH;
        float BACKGROUND_HEIGHT = BACKGROUND_WIDTH * backgroundRegion.getHeight()  / backgroundRegion.getWidth();

        Sprite backgroundSprite1 = new Sprite(0, 0, this.backgroundRegion, getVertexBufferObjectManager());
        backgroundSprite1.setWidth(BACKGROUND_WIDTH);
        backgroundSprite1.setHeight(BACKGROUND_HEIGHT);

        Sprite backgroundSprite2 = new Sprite(0, 0, this.backgroundRegion, getVertexBufferObjectManager());
        backgroundSprite2.setWidth(BACKGROUND_WIDTH);
        backgroundSprite2.setHeight(BACKGROUND_HEIGHT);

        final float BACK1_FROM_Y = -backgroundSprite1.getHeight() + CAMERA_HEIGHT;
        final float BACK1_TO_Y = CAMERA_HEIGHT;

        final float BACK2_FROM_Y = BACK1_FROM_Y - backgroundSprite2.getHeight();
        final float BACK2_TO_Y = BACK1_TO_Y - backgroundSprite2.getHeight();

        IEntityModifier.IEntityModifierListener iEntityModifierListener = new IEntityModifier.IEntityModifierListener() {
            @Override
            public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {

            }

            @Override
            public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {

            }
        };

        backgroundSprite1.registerEntityModifier(new MoveYModifier(5f, BACK1_FROM_Y, BACK1_TO_Y));
        backgroundSprite2.registerEntityModifier(new MoveYModifier(5f, BACK2_FROM_Y, BACK2_TO_Y));

        scene.attachChild(backgroundSprite1);
        scene.attachChild(backgroundSprite2);

    }

    private void calculateTouchMoveDiff(TouchEvent pSceneTouchEvent) {
        if(previousTouchMoveCoords[0] == 0 && previousTouchMoveCoords[1] == 0) {
            touchMoveDiff[0] = 0;
            touchMoveDiff[1] = 0;
        }
        else {
            touchMoveDiff[0] = pSceneTouchEvent.getX() - previousTouchMoveCoords[0];
            touchMoveDiff[1] = pSceneTouchEvent.getY() - previousTouchMoveCoords[1];
        }

        previousTouchMoveCoords[0] = pSceneTouchEvent.getX();
        previousTouchMoveCoords[1] = pSceneTouchEvent.getY();
    }

    private void resetMoveDiff() {
        previousTouchMoveCoords[0] = 0;
        previousTouchMoveCoords[1] = 0;
    }

    private void getSceneAbstractSquares() {

    }

    @Override
    public void onUpdate(float pSecondsElapsed) {
        float ms = pSecondsElapsed * 1000;
        squareLogic.update(ms);
    }

    @Override
    public void reset() {

    }

    public ITextureRegion getSquareEnemyRegion() {
        return squareEnemyRegion;
    }

    public void setSquareEnemyRegion(ITextureRegion squareEnemyRegion) {
        this.squareEnemyRegion = squareEnemyRegion;
    }

    public ITextureRegion getSquareFriendRegion() {
        return squareFriendRegion;
    }

    public void setSquareFriendRegion(ITextureRegion squareFriendRegion) {
        this.squareFriendRegion = squareFriendRegion;
    }

    public ITextureRegion getSquareLifeRegion() {
        return squareLifeRegion;
    }

    public void setSquareLifeRegion(ITextureRegion squareLifeRegion) {
        this.squareLifeRegion = squareLifeRegion;
    }

    public ITextureRegion getSquarePowerRegion() {
        return squarePowerRegion;
    }

    public void setSquarePowerRegion(ITextureRegion squarePowerRegion) {
        this.squarePowerRegion = squarePowerRegion;
    }

    public PlayerSquare getPlayerSquare() {
        return playerSquare;
    }

    public Scene getScene() {
        return scene;
    }

    public ITextureRegion getLifeLeftRegion() {
        return lifeLeftRegion;
    }

    public ITextureRegion getLifeRightRegion() {
        return lifeRightRegion;
    }

    public void setLifeLeftRegion(ITextureRegion lifeLeftRegion) {
        this.lifeLeftRegion = lifeLeftRegion;
    }

    public void setLifeRightRegion(ITextureRegion lifeRightRegion) {
        this.lifeRightRegion = lifeRightRegion;
    }

    @Override
    public void onTimePassed(TimerHandler pTimerHandler) {

        if(powerTimer == pTimerHandler) {
            //power mode ends

        }
    }
}