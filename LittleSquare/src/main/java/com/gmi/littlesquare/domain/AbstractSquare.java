package com.gmi.littlesquare.domain;

import com.gmi.littlesquare.GameActivity;
import com.gmi.littlesquare.constant.SquareDifficulty;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.CubicBezierCurveMoveModifier;
import org.andengine.entity.modifier.IEntityModifier;
import org.andengine.entity.modifier.PathModifier;
import org.andengine.entity.modifier.ScaleAtModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.util.modifier.IModifier;

/**
 * Created by pablo on 14/09/14.
 */
public abstract class AbstractSquare extends Sprite implements IEntityModifier.IEntityModifierListener {

    public static final Object ABSTRACT_SQUARE_ID = new Object();
    protected GameActivity gameActivity;
    protected SquareDifficulty difficulty;

    public AbstractSquare(GameActivity gameActivity, float pX, float pY, ITextureRegion pTextureRegion, SquareDifficulty difficulty) {
        super(pX, pY, pTextureRegion, gameActivity.getVertexBufferObjectManager());
        setUserData(ABSTRACT_SQUARE_ID);
        this.gameActivity = gameActivity;
        this.difficulty = difficulty;
    }

    public abstract void collides(AbstractSquare abstractSquare);

    protected void animateScaleLoop() {
        ScaleAtModifier makeSmallMod = new ScaleAtModifier(0.5f, 1, 0.5f, getWidth()/2f, getHeight()/2f);
        ScaleAtModifier makeBigMod = new ScaleAtModifier(0.5f, 0.5f, 1f, getWidth()/2f, getHeight()/2f);
        SequenceEntityModifier sequenceEntityModifier = new SequenceEntityModifier(makeSmallMod, makeBigMod) {
            @Override
            protected void onModifierFinished(IEntity pItem) {
                super.reset();
            }
        };
        registerEntityModifier(sequenceEntityModifier);
    }

    protected void animateScaleDown(float duration, IModifier.IModifierListener modifierListener) {
        ScaleAtModifier makeSmallMod = new ScaleAtModifier(duration, 1, 0f, getWidth()/2f, getHeight()/2f);
        makeSmallMod.setAutoUnregisterWhenFinished(true);
        makeSmallMod.addModifierListener(modifierListener);
        registerEntityModifier(makeSmallMod);
    }

    public CubicBezierCurveMoveModifier createSquarePath() {

        float origenX = 0f;
        float origenY = 0f;
        float destinoX = 0f;
        float destinoY = 0f;
        float tiempoMaximoRecorrido = 0f;

        /**
         * Aleatoriamente elegimos un lado para salir
         */
        byte origenLado = (byte)(Math.random() * 4f);

        origenLado = 0;

        switch (origenLado) {
            case 0:
                origenX = (float)(Math.random() * GameActivity.CAMERA_WIDTH);
                origenY = -getHeight();
                destinoX = origenX;
                destinoY = GameActivity.CAMERA_HEIGHT;
                break;
            case 1:
                origenX = GameActivity.CAMERA_WIDTH + getWidth();
                origenY = (float)(Math.random() * GameActivity.CAMERA_HEIGHT);
                destinoX = -getWidth()-getWidth();
                destinoY = origenY;
                break;
            case 2:
                origenX = (int)(Math.random() * (float)GameActivity.CAMERA_WIDTH);
                origenY = GameActivity.CAMERA_HEIGHT + getHeight();
                destinoX = origenX;
                destinoY = -getHeight()-getHeight();
                break;
            case 3:
                origenX = -getWidth();
                origenY = (float)(Math.random() * GameActivity.CAMERA_HEIGHT);
                destinoX = GameActivity.CAMERA_WIDTH + getWidth();
                destinoY = origenY;
                break;
            default:
                break;
        }

        CubicBezierCurveMoveModifier cubicBezierCurveMoveModifier = null;

        difficulty = SquareDifficulty.FACIL;

        switch (difficulty) {
            case FACIL:
                tiempoMaximoRecorrido = (3000f + ((float)Math.random() * 1500f)) / 1000f;
                if(origenLado == 0 || origenLado == 2) {
                    float divisionesY = GameActivity.CAMERA_WIDTH / 3;
//                    recorrido = new Curve(new Vector2f(origenX, origenY), new Vector2f(origenX, divisionesY), new Vector2f(origenX, divisionesY * 2), new Vector2f(destinoX, destinoY));
                    cubicBezierCurveMoveModifier = new CubicBezierCurveMoveModifier(tiempoMaximoRecorrido, origenX, origenY, origenX, divisionesY, origenX, divisionesY * 2f, destinoX, destinoY, this);
                }
                else if(origenLado == 1 || origenLado == 3) {
                    //division exacta del ancho en 3 partes
                    float divisionesX = GameActivity.CAMERA_WIDTH / 3f;
                    cubicBezierCurveMoveModifier = new CubicBezierCurveMoveModifier(tiempoMaximoRecorrido, origenX, origenY, GameActivity.CAMERA_WIDTH - divisionesX, origenY, GameActivity.CAMERA_WIDTH - (divisionesX * 2f), origenY, destinoX, destinoY, this);
                }
                break;
            case RELATIVAMENTE_FACIL:
                tiempoMaximoRecorrido = (2000f + ((float)Math.random() * 1100f)) / 1000f;
                if(origenLado == 0 || origenLado == 2) {
                    //division exacta de la altura en 3 partes
                    float divisionesY = GameActivity.CAMERA_HEIGHT / 3f;
                    cubicBezierCurveMoveModifier = new CubicBezierCurveMoveModifier(tiempoMaximoRecorrido, origenX, origenY, origenX, divisionesY, origenX, divisionesY * 2f, destinoX, destinoY, this);
//                    recorrido = new Curve(new Vector2f(origenX, origenY), new Vector2f(origenX, divisionesY), new Vector2f(origenX, divisionesY * 2), new Vector2f(destinoX, destinoY));
                }
                else if(origenLado == 1 || origenLado == 3) {
                    //division exacta del ancho en 3 partes
                    float divisionesX = GameActivity.CAMERA_WIDTH / 3f;
                    cubicBezierCurveMoveModifier = new CubicBezierCurveMoveModifier(tiempoMaximoRecorrido, origenX, origenY, GameActivity.CAMERA_WIDTH - divisionesX, origenY, GameActivity.CAMERA_WIDTH - (divisionesX * 2f), origenY, destinoX, destinoY, this);
//                    recorrido = new Curve(new Vector2f(origenX, origenY), new Vector2f(Configuracion.SCREEN_WIDTH - divisionesX, origenY), new Vector2f(Configuracion.SCREEN_WIDTH - (divisionesX * 2), origenY), new Vector2f(destinoX, destinoY));
                }
                break;
            case MEDIANO:
                tiempoMaximoRecorrido = (2000f + ((float)Math.random() * 1000f)) / 1000f;
                if(origenLado == 0 || origenLado == 2) {
                    //division exacta de la altura en 3 partes
                    float divisionesX = GameActivity.CAMERA_WIDTH / 3f;
                    float divisionesY = GameActivity.CAMERA_HEIGHT / 3f;

                    //lado vuelque aleatorio
                    boolean vuelque = (Math.random() * 100f) < 50f ? true : false;
                    if(vuelque) {
                        divisionesX = -divisionesX;
                    }
//                    recorrido = new Curve(new Vector2f(origenX, origenY), new Vector2f(origenX + (divisionesX / 2), divisionesY), new Vector2f(origenX + (divisionesX / 2), divisionesY * 2), new Vector2f(destinoX, destinoY));
                    cubicBezierCurveMoveModifier = new CubicBezierCurveMoveModifier(tiempoMaximoRecorrido, origenX, origenY, origenX + (divisionesX / 2f), divisionesY, origenX + (divisionesX / 2f), divisionesY * 2f, destinoX, destinoY, this);
                }
                else if(origenLado == 1 || origenLado == 3) {
                    //division exacta de la altura en 3 partes
                    float divisionesY = GameActivity.CAMERA_HEIGHT / 3f;

                    //lado vuelque aleatorio
                    boolean vuelque = (Math.random() * 100f) < 50f ? true : false;
                    if(vuelque) {
                        divisionesY = -divisionesY;
                    }

                    //division exacta del ancho en 3 partes
                    float divisionesX = GameActivity.CAMERA_WIDTH / 3f;
//                    recorrido = new Curve(new Vector2f(origenX, origenY), new Vector2f(Configuracion.SCREEN_WIDTH - divisionesX, origenY + divisionesY), new Vector2f(Configuracion.SCREEN_WIDTH - (divisionesX * 2), origenY + divisionesY), new Vector2f(destinoX, destinoY));
                    cubicBezierCurveMoveModifier = new CubicBezierCurveMoveModifier(tiempoMaximoRecorrido, origenX, origenY, GameActivity.CAMERA_WIDTH - divisionesX, origenY + divisionesY, GameActivity.CAMERA_WIDTH - (divisionesX * 2f), origenY + divisionesY, destinoX, destinoY, this);
                }
                break;
            case DIFICIL:
                tiempoMaximoRecorrido = (2000f + ((float)Math.random() * 500f)) / 1000f;
                if(origenLado == 0 || origenLado == 2) {

                    //division exacta de la altura en 3 partes
                    float divisionesX = GameActivity.CAMERA_WIDTH / 3f;
                    float divisionesY = GameActivity.CAMERA_HEIGHT / 3f;

                    //lado vuelque aleatorio
                    boolean vuelque = (Math.random() * 100f) < 50f ? true : false;
                    if(vuelque) {
                        divisionesX = -divisionesX;
                    }

//                    recorrido = new Curve(new Vector2f(origenX, origenY), new Vector2f(origenX + (divisionesX / 2), divisionesY), new Vector2f(origenX - (divisionesX / 2), divisionesY * 2), new Vector2f(destinoX, destinoY));
                    cubicBezierCurveMoveModifier = new CubicBezierCurveMoveModifier(tiempoMaximoRecorrido, origenX, origenY, origenX + (divisionesX / 2f), divisionesY, origenX - (divisionesX / 2f), divisionesY * 2f, destinoX, destinoY, this);
                }
                else if(origenLado == 1 || origenLado == 3) {

                    //division exacta de la altura en 3 partes
                    float divisionesY = GameActivity.CAMERA_HEIGHT / 3f;

                    //lado vuelque aleatorio
                    boolean vuelque = (Math.random() * 100f) < 50f ? true : false;
                    if(vuelque) {
                        divisionesY = -divisionesY;
                    }

                    //division exacta del ancho en 3 partes
                    float divisionesX = GameActivity.CAMERA_WIDTH / 3f;
//                    recorrido = new Curve(new Vector2f(origenX, origenY), new Vector2f(Configuracion.SCREEN_WIDTH - divisionesX, origenY + divisionesY), new Vector2f(Configuracion.SCREEN_WIDTH - (divisionesX * 2), origenY - divisionesY), new Vector2f(destinoX, destinoY));
                    cubicBezierCurveMoveModifier = new CubicBezierCurveMoveModifier(tiempoMaximoRecorrido, origenX, origenY, GameActivity.CAMERA_WIDTH - divisionesX, origenY + divisionesY, GameActivity.CAMERA_WIDTH - (divisionesX * 2f), origenY - divisionesY, destinoX, destinoY, this);
                }

                break;
            case MUY_DIFICIL:
                tiempoMaximoRecorrido = (1500f + ((float)Math.random() * 500f)) / 1000f;
                if(origenLado == 0 || origenLado == 2) {

                    //division exacta de la altura en 3 partes
                    float divisionesX = GameActivity.CAMERA_WIDTH / 3f;
                    float divisionesY = GameActivity.CAMERA_HEIGHT / 3f;

                    //lado vuelque aleatorio
                    boolean vuelque = (Math.random() * 100f) < 50f ? true : false;
                    if(vuelque) {
                        divisionesX = -divisionesX;
                    }

//                    recorrido = new Curve(new Vector2f(origenX, origenY), new Vector2f(origenX + (divisionesX * 2), divisionesY), new Vector2f(origenX - (divisionesX * 2), divisionesY * 2), new Vector2f(destinoX, destinoY));
                    cubicBezierCurveMoveModifier = new CubicBezierCurveMoveModifier(tiempoMaximoRecorrido, origenX, origenY, origenX + (divisionesX * 2f), divisionesY, origenX - (divisionesX * 2f), divisionesY * 2f, destinoX, destinoY, this);
                }
                else if(origenLado == 1 || origenLado == 3) {

                    //division exacta de la altura en 3 partes
                    float divisionesY = GameActivity.CAMERA_HEIGHT / 3f;

                    //lado vuelque aleatorio
                    boolean vuelque = (Math.random() * 100f) < 50f ? true : false;
                    if(vuelque) {
                        divisionesY = -divisionesY;
                    }

                    //division exacta del ancho en 3 partes
                    int divisionesX = (int)(GameActivity.CAMERA_WIDTH / 3f);
//                    recorrido = new Curve(new Vector2f(origenX, origenY), new Vector2f(Configuracion.SCREEN_WIDTH - divisionesX, origenY + (divisionesY * 2)), new Vector2f(Configuracion.SCREEN_WIDTH - (divisionesX * 2), origenY - (divisionesY * 2)), new Vector2f(destinoX, destinoY));
                    cubicBezierCurveMoveModifier = new CubicBezierCurveMoveModifier(tiempoMaximoRecorrido, origenX, origenY, GameActivity.CAMERA_WIDTH - divisionesX, origenY + (divisionesY * 2f), GameActivity.CAMERA_WIDTH - (divisionesX * 2f), origenY - (divisionesY * 2f), destinoX, destinoY, this);
                }

                break;
            case DIFICILISIMO:
                tiempoMaximoRecorrido = (1200f + ((float)Math.random() * 500f)) / 1000f;
                if(origenLado == 0 || origenLado == 2) {

                    //division exacta de la altura en 3 partes
                    float divisionesX = GameActivity.CAMERA_WIDTH / 3f;
                    float divisionesY = GameActivity.CAMERA_HEIGHT / 3f;

                    //lado vuelque aleatorio
                    boolean vuelque = (Math.random() * 100f) < 50f ? true : false;
                    if(vuelque) {
                        divisionesX = -divisionesX;
                    }

//                    recorrido = new Curve(new Vector2f(origenX, origenY), new Vector2f(origenX + (divisionesX * 2), divisionesY), new Vector2f(origenX - (divisionesX), divisionesY * 2), new Vector2f(destinoX, destinoY));
                    cubicBezierCurveMoveModifier = new CubicBezierCurveMoveModifier(tiempoMaximoRecorrido, origenX, origenY, origenX + (divisionesX * 2f), divisionesY, origenX - divisionesX, divisionesY * 2f, destinoX, destinoY, this);
                }
                else if(origenLado == 1 || origenLado == 3) {

                    //division exacta de la altura en 3 partes
                    int divisionesY = (int)(GameActivity.CAMERA_HEIGHT / 3f);

                    //lado vuelque aleatorio
                    boolean vuelque = (Math.random() * 100f) < 50f ? true : false;
                    if(vuelque) {
                        divisionesY = -divisionesY;
                    }

                    //division exacta del ancho en 3 partes
                    int divisionesX = (int)(GameActivity.CAMERA_WIDTH / 3f);
//                    recorrido = new Curve(new Vector2f(origenX, origenY), new Vector2f(Configuracion.SCREEN_WIDTH - divisionesX, origenY + (divisionesY * 2)), new Vector2f(Configuracion.SCREEN_WIDTH - (divisionesX), origenY - (divisionesY * 2)), new Vector2f(destinoX, destinoY));
                    cubicBezierCurveMoveModifier = new CubicBezierCurveMoveModifier(tiempoMaximoRecorrido, origenX, origenY, GameActivity.CAMERA_WIDTH - divisionesX, origenY + (divisionesY * 2f), GameActivity.CAMERA_WIDTH - divisionesX, origenY - (divisionesY * 2), destinoX, destinoY, this);
                }

                break;
            default:
                break;
        }

        return cubicBezierCurveMoveModifier;
    }

    public void remove() {
        gameActivity.getEngine().runOnUpdateThread(new Runnable() {
            @Override
            public void run() {
                detachSelf();
            }
        });
    }
}