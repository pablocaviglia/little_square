package com.gmi.littlesquare.domain;

import com.gmi.littlesquare.GameActivity;

import org.andengine.entity.IEntity;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.modifier.IModifier;

/**
 * Created by pablo on 14/09/14.
 */
public class PlayerSquare extends AbstractSquare {

    public PlayerSquare(GameActivity gameActivity, ITextureRegion pTextureRegion) {
        super(gameActivity, 0, 0, pTextureRegion, null);
        float squareSize = (GameActivity.CAMERA_WIDTH + GameActivity.CAMERA_HEIGHT) * 0.05f;
        setWidth(squareSize);
        setHeight(squareSize);
        setX((GameActivity.CAMERA_WIDTH - getWidth())/2f);
        setY((GameActivity.CAMERA_HEIGHT - getHeight())/2f);
    }

    public void move(float x, float y) {
        setX(getX() + x);
        setY(getY() + y);
    }

    @Override
    public void setX(float x) {
        if(verifyMoveX(x)) {
            super.setX(x);
        }
        else {
            if(x < 0) {
                super.setX(0);
            }
            else {
                super.setX(GameActivity.CAMERA_WIDTH-getWidth());
            }
        }
    }

    @Override
    public void setY(float y) {
        if(verifyMoveY(y)) {
            super.setY(y);
        }
        else {
            if(y < 0) {
                super.setY(0);
            }
            else {
                super.setY(GameActivity.CAMERA_HEIGHT-getHeight());
            }
        }
    }

    private boolean verifyMoveX(float x) {
        if(x >= 0 && x <= (GameActivity.CAMERA_WIDTH-getWidth())) {
            return true;
        }
        return false;
    }

    private boolean verifyMoveY(float y) {
        if(y >= 0 && y <= (GameActivity.CAMERA_HEIGHT-getHeight())) {
            return true;
        }
        return false;
    }

    @Override
    public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {

    }

    @Override
    public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {

    }

    @Override
    public void collides(AbstractSquare abstractSquare) {

    }
}
