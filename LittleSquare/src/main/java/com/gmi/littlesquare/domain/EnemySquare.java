package com.gmi.littlesquare.domain;

import com.gmi.littlesquare.GameActivity;
import com.gmi.littlesquare.constant.SquareDifficulty;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.CubicBezierCurveMoveModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;
import org.andengine.util.modifier.IModifier;

/**
 * Created by pablo on 14/09/14.
 */
public class EnemySquare extends AbstractSquare {

    public EnemySquare(GameActivity gameActivity, SquareDifficulty difficulty) {
        super(gameActivity, 0, 0, gameActivity.getSquareEnemyRegion(), difficulty);
        float squareSize = (GameActivity.CAMERA_WIDTH + GameActivity.CAMERA_HEIGHT) * 0.035f;
        setWidth(squareSize);
        setHeight(squareSize);

        CubicBezierCurveMoveModifier cubicBezierCurveMoveModifier = createSquarePath();
        cubicBezierCurveMoveModifier.setAutoUnregisterWhenFinished(true);
        registerEntityModifier(cubicBezierCurveMoveModifier);
    }

    @Override
    public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {

    }

    @Override
    public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
        remove();
    }

    @Override
    public void collides(AbstractSquare abstractSquare) {

    }
}
