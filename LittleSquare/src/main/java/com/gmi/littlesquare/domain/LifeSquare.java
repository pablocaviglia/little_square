package com.gmi.littlesquare.domain;

import com.gmi.littlesquare.GameActivity;
import com.gmi.littlesquare.constant.SquareDifficulty;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.CubicBezierCurveMoveModifier;
import org.andengine.util.modifier.IModifier;

/**
 * Created by pablo on 16/09/14.
 */
public class LifeSquare extends AbstractSquare {

    public LifeSquare(GameActivity gameActivity, SquareDifficulty difficulty) {
        super(gameActivity, 0, 0, gameActivity.getSquareLifeRegion(), difficulty);
        float squareSize = (GameActivity.CAMERA_WIDTH + GameActivity.CAMERA_HEIGHT) * 0.035f;
        setWidth(squareSize);
        setHeight(squareSize);
        animateScaleLoop();

        CubicBezierCurveMoveModifier cubicBezierCurveMoveModifier = createSquarePath();
        cubicBezierCurveMoveModifier.setAutoUnregisterWhenFinished(true);
        registerEntityModifier(cubicBezierCurveMoveModifier);
    }

    @Override
    public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {

    }

    @Override
    public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
        remove();
    }

    @Override
    public void collides(AbstractSquare abstractSquare) {

    }
}
