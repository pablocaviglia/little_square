package com.gmi.littlesquare.domain;

import com.gmi.littlesquare.GameActivity;

import org.andengine.engine.camera.hud.HUD;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.ScaleAtModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.sprite.Sprite;

/**
 * Created by pablo on 16/09/14.
 */
public class GameHUD extends HUD {

    private GameActivity gameActivity;

    public GameHUD(GameActivity gameActivity) {
        this.gameActivity = gameActivity;
    }

    public void addLifes(int lifes, float duration) {

        float LIFE_SPRITE_WIDTH = 16;
        float LIFE_SPRITE_HEIGHT = 32;
        float currentXPosition = GameActivity.CAMERA_WIDTH - LIFE_SPRITE_WIDTH - 8;
        float currentAppearingTime = 0f;

        for(int i=1; i<=lifes; i++) {

            boolean leftSide = i % 2 == 0 ? true : false;

            Sprite lifeSprite = new Sprite(0, 0, leftSide ? gameActivity.getLifeLeftRegion() : gameActivity.getLifeRightRegion(), gameActivity.getVertexBufferObjectManager());
            lifeSprite.setWidth(LIFE_SPRITE_WIDTH);
            lifeSprite.setHeight(LIFE_SPRITE_HEIGHT);
            lifeSprite.setX(currentXPosition);
            lifeSprite.setY(5);

            SequenceEntityModifier sequenceEntityModifier = new SequenceEntityModifier(new DelayModifier(currentAppearingTime), new ScaleAtModifier(1f, 0f, 1f, LIFE_SPRITE_WIDTH/2f, LIFE_SPRITE_HEIGHT/2f));
            lifeSprite.registerEntityModifier(sequenceEntityModifier);
            attachChild(lifeSprite);

            //change the position
            //for the next sprite
            currentXPosition -= (lifeSprite.getWidth() + (leftSide ? 5 : 0));
            currentAppearingTime += (leftSide ? 0.2f : 0f) ;
        }
    }
}
