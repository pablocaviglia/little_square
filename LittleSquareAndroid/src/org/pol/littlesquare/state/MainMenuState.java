package org.pol.littlesquare.state;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;
import org.pol.littlesquare.LittleSquareGame;
import org.pol.littlesquare.configuracion.Configuracion;
import org.pol.littlesquare.logica.ManejadorCuadrados;
import org.pol.littlesquare.ui.Boton;
import org.pol.littlesquare.ui.event.UIEvent;
import org.pol.littlesquare.util.Timer;

public class MainMenuState extends AbstractState {

	/**
	 * Logo del juego
	 */
	private Image logo;
	
	/**
	 * Imagen del perfil del jugador 
	 * de facebook. Usado para el highscore
	 * y para la vida
	 */
	public static Image imagenPerfilJugador;
	public static Image imagenPerfilJugadorVida;

	//lista con los botones usados
	//en la ui de este estado
	private List<Boton> botones = new ArrayList<Boton>();
	
	//botones independientes
	private Boton nuevoJuegoBoton;
	private Boton ayudaBoton;
	
	//usado para animar la rotacion del logo
	private boolean rotacionHabilitada;
	private boolean rotacionFinalizada;
	private final int TIEMPO_ROTACION_LOGO = 2000;
	private final int TIEMPO_DETENCION_LOGO = 8000;
	
    int stateID = -1;
    
    //usado como color de fondo
    //del menu de seleccion 
    private Color grisTransparente = new Color(0.5f, 0.5f, 0.5f, 0.75f);
    
    private final int MENU_WIDTH = 450;
    private final int MENU_HEIGHT = 300;
    
	/**
	 * Maneja la salida de los cuadrados en
	 * base a los que se van agarrando y al
	 * tiempo que va pasando
	 */
	private ManejadorCuadrados manejadorCuadrados;
	
	//timer que controla los tiempos
	//de rotacion del logo
	private Timer timerRotacionLogo;
	
	//tiempo de ajuste del cursor para
	//que no quede en cualquier lugar
	//cuando se inicia el estado
	private int tiempoAjusteCursor;
	
    public MainMenuState(int stateID) {
       this.stateID = stateID;
    }

    @Override
    public int getID() {
        return stateID;
    }

    @Override
    public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {

    }
    
    public void renderizar(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {

		//pintamos el fondo
		g.setColor(Color.white);
		g.fillRect(0, 0, gc.getWidth(), gc.getHeight());
		
		manejadorCuadrados.render(gc, sbg, g);
		
		//seteamos el color con transparencia
		//para dibujar arriba el menu del juego
		g.setColor(grisTransparente);
		g.fillRect((Configuracion.SCREEN_WIDTH - MENU_WIDTH) / 2 , ((Configuracion.SCREEN_HEIGHT - MENU_HEIGHT) / 2) + 30 , MENU_WIDTH, MENU_HEIGHT);

		//dibujamos el logo
		g.drawImage(logo, (Configuracion.SCREEN_WIDTH - logo.getWidth()) / 2, 20);
		
		//renderizamos los botones
		for(Boton boton : botones) {
			boton.render(g);
		}
    }

    public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
    	
    	super.update(gc, sbg, delta);
    	
    	//ajustamos el cursor durante
    	//un segundo para no tener 
    	//comportamientos raros
    	if(tiempoAjusteCursor > 0) {
    		
    		tiempoAjusteCursor -= delta;
    	}
    	
    	//maneja la salida 
    	//de los cuadrados
    	manejadorCuadrados.update(gc, sbg, delta);

    	//logica de rotacion del logo
		if(rotacionHabilitada) {
			logo.rotate(20f);
		}
		else {
			if(!rotacionFinalizada) {
				logo.rotate(20f);
				if(logo.getRotation() == 0) {
					rotacionFinalizada = true;
				}
			}
		}
		
		//si cumplimos el tiempo de 
		//rotacion o detencion
		if(timerRotacionLogo.action(delta)) {
			if(timerRotacionLogo.getDelay() == TIEMPO_DETENCION_LOGO) {
				timerRotacionLogo.setDelay(TIEMPO_ROTACION_LOGO);
				rotacionHabilitada = true;
				rotacionFinalizada = false;
			}
			else {
				rotacionHabilitada = false;
				timerRotacionLogo.setDelay(TIEMPO_DETENCION_LOGO);
			}
		}
		
		//actualizamos la logica
		//de los botones
		for(Boton boton : botones) {
			boton.update(delta);
		}
    }

	@Override
	protected void entrar(GameContainer container, StateBasedGame game) throws SlickException {

		//tiempo de ajuste del cursor
		tiempoAjusteCursor = 1000;
		
    	//creamos el modulo encargado
    	//de manejar la salida de los
    	//cuadrados por los diferentes
    	//lados de la pantalla
    	manejadorCuadrados = new ManejadorCuadrados();
    	
    	//inicializamos los cuadrados
    	//del manejador y no le seteamos
    	//el del jugador ya que va a correr
    	//en modo demo
    	manejadorCuadrados.inicializarCuadrados(null);
    	
    	//refernciamos la imagen del logo
    	logo = new Image("gfx/little_square_logo.png");
    	
    	//timer de rotacion
    	timerRotacionLogo = new Timer(TIEMPO_DETENCION_LOGO);
    	
    	nuevoJuegoBoton = new Boton((Configuracion.SCREEN_WIDTH - 250) / 2, 180, 250, 65, "NUEVO JUEGO");
    	ayudaBoton = new Boton((Configuracion.SCREEN_WIDTH - 250) / 2, 290, 250, 65, "AYUDA");
    	botones.add(nuevoJuegoBoton);
    	botones.add(ayudaBoton);
    	nuevoJuegoBoton.addEventListener(this);
    	ayudaBoton.addEventListener(this);
    	
	}

	@Override
	protected void salir(GameContainer container, StateBasedGame game) {
		
	}
	
	@Override
    public void touchMovido(int x, int y) {
		
	}
	
	@Override
	public void touchArriba(int x, int y) {
		
		nuevoJuegoBoton.touchDown(x, y);
		ayudaBoton.touchDown(x, y);
	}
	
	@Override
	public void eventoGenerado(UIEvent event) {
		
		if(event.generador == nuevoJuegoBoton) {
			AyudaState.continuaHaciaJuego = true;
			littleSquareGame.enterState(LittleSquareGame.AYUDASTATE, new FadeOutTransition(), new FadeInTransition());
		}
		else if(event.generador == ayudaBoton) {
			AyudaState.continuaHaciaJuego = false;
			littleSquareGame.enterState(LittleSquareGame.AYUDASTATE, new FadeOutTransition(), new FadeInTransition());
		}
	}
	
	@Override
	public void dobleTouch() {

	}
	
	@Override
	public boolean keyTyped(char arg0) {
		
		return false;
	}
	
	@Override
	public boolean keyUp(int keyCode) {
		
		return true;
	}
}