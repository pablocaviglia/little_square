package org.pol.littlesquare.state;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;
import org.pol.littlesquare.LittleSquareGame;
import org.pol.littlesquare.audio.SfxPlayer;
import org.pol.littlesquare.configuracion.Configuracion;
import org.pol.littlesquare.dominio.Cuadrado;
import org.pol.littlesquare.dominio.CuadradoAmigo;
import org.pol.littlesquare.dominio.CuadradoEnemigo;
import org.pol.littlesquare.dominio.CuadradoJugador;
import org.pol.littlesquare.logica.ManejadorCuadrados;
import org.pol.littlesquare.ui.Boton;
import org.pol.littlesquare.ui.ImagenApareciendo;
import org.pol.littlesquare.ui.event.UIEvent;
import org.pol.littlesquare.util.Timer;

public class GameplayState extends AbstractState {

    int stateID = -1;
    
    //usado como color de fondo
    //del menu de seleccion 
    private Color grisTransparente = new Color(0.5f, 0.5f, 0.5f, 0.75f);
    
    private int MENU_HIGHSCORE_WIDTH = 350;
    private int MENU_HIGHSCORE_HEIGHT = 300;
    
    /**
     * Score actual del jugador
     */
    public static int score;
    
    /**
     * Cantidad de frenesis 
     * con los que cuenta el
     * usuario
     */
    public static int frenesis;
    
    public static int tiempoActualFrenesi;
    public final int TIEMPO_TOTAL_FRENESI = 10000;
    
    private Color barraTiempoFrenesiColor;
    
    /**
     * Timers que cuentan durante la reproduccion
     * de la cancion de frenesi los golpes que van
     * apareciendo en pantalla
     */
    private Timer timerFrenesiGolpe1 = new Timer(2900);
    private Timer timerFrenesiGolpe2 = new Timer(3200);
    private Timer timerFrenesiGolpe3 = new Timer(4200);
    private Timer timerFrenesiGolpe4 = new Timer(5200);
    
    /**
     * Cantidad de vidas del jugador
     */
    public static int vidasRestantes;
    
    /**
     * Tiempo total actual de la partida
     */
    public long tiempoTotalPartida;
	
	//referencia directa al cuadrado del jugador
	private CuadradoJugador cuadradoJugador;
	
	/**
	 * Maneja la salida de los cuadrados en
	 * base a los que se van agarrando y al
	 * tiempo que va pasando
	 */
	private ManejadorCuadrados manejadorCuadrados;
	
	/**
	 * Icono de la vida
	 */
	private Image vidaImage;
	
	/**
	 * Icono del frenesi
	 */
	private Image frenesiImage;
	
	/**
	 * Logo del highscore
	 */
	private Image highscoreImage;
	
	//lista con los botones usados
	//en la ui de este estado
	private List<Boton> botones = new ArrayList<Boton>();
	
	//botones independientes
	private Boton reiniciarJuegoBoton;
	private Boton resumirBoton;
	private Boton salirBoton;
	
	/**
	 * Imagenes que aparecen desde
	 * el fondo relacionadas a batman
	 */
	private ImagenApareciendo powImagenApareciendo1;
	private ImagenApareciendo powImagenApareciendo2;
	private ImagenApareciendo powImagenApareciendo3;
	private ImagenApareciendo powImagenApareciendo4;
	
	/**
	 * Medidas del texto gameover
	 */
	private int anchoTextoGameOver;
	private int altoTextoGameOver;
	
	/**
	 * Texto GAME OVER
	 */
	private String gameOverTxt = "GAME OVER !";
	
	/**
	 * Timer para el cartel de gameover
	 */
	private Timer gameOverTimer;
	private boolean mostrarCartelGameOver;
	private byte repeticionesGameOver;
	
	/**
	 * Posibles estados durante el juego
	 */
	private enum STATES {
        START_GAME_STATE, PLAYING_GAME_STATE, PAUSE_GAME_STATE, HIGHSCORE_STATE, GAME_OVER_STATE
    }

	/**
	 * Estado actual del juego
	 */
	private static STATES currentState = null;
	
	/**
	 * Constructor
	 * @param stateID
	 */
    public GameplayState(int stateID) {
       this.stateID = stateID;
    }

    @Override
    public int getID() {
        return stateID;
    }

    public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {

    	//creamos el modulo encargado
    	//de manejar la salida de los
    	//cuadrados por los diferentes
    	//lados de la pantalla
    	manejadorCuadrados = new ManejadorCuadrados();
    	
    }
    
	@Override
	protected void entrar(GameContainer container, StateBasedGame game) throws SlickException {
	
		/**
		 * Little FIX ;)
		 */
		Cuadrado.gameplayState = this;
		
		//ancho y alto del 
		//texto de game over
		anchoTextoGameOver = LittleSquareGame.angelCodeFont.getWidth(gameOverTxt);
		altoTextoGameOver = LittleSquareGame.angelCodeFont.getHeight(gameOverTxt);
		
        //cargamos imagenes
        vidaImage = new Image("gfx/vida_icon.png");
        frenesiImage = new Image("gfx/frenesi_icon.png").getScaledCopy(0.22f);
        highscoreImage = new Image("gfx/highscore.png").getScaledCopy(0.7f);
        
    	//cambiamos el estado del juego
     	currentState = STATES.START_GAME_STATE;

        //cargamos sonidos
        SfxPlayer.loadSounds(new byte[]{SfxPlayer.OUCH, SfxPlayer.YES, SfxPlayer.VIDA, SfxPlayer.FRENETICO_MUSICA, SfxPlayer.FRENETICO});
        
    	reiniciarJuegoBoton = new Boton((Configuracion.SCREEN_WIDTH - 250) / 2, 70, 250, 65, "REINICIAR JUEGO");
    	resumirBoton = new Boton((Configuracion.SCREEN_WIDTH - 250) / 2, 180, 250, 65, "RESUMIR");
    	salirBoton = new Boton((Configuracion.SCREEN_WIDTH - 250) / 2, 290, 250, 65, "SALIR");
    	botones.add(reiniciarJuegoBoton);
    	botones.add(resumirBoton);
    	botones.add(salirBoton);
    	reiniciarJuegoBoton.addEventListener(this);
    	resumirBoton.addEventListener(this);
    	salirBoton.addEventListener(this);
    	
    	//instanciamos el color
    	//de la barra de tiempo
    	//del frenesi
    	barraTiempoFrenesiColor = new Color(Color.black);
        
    	Image powImage1 = new Image("gfx/frenesi_icon.png");
    	powImagenApareciendo1 = new ImagenApareciendo(330, (Configuracion.SCREEN_HEIGHT - powImage1.getHeight()) / 2, powImage1, 1000, -30f);
    	Image powImage2 = new Image("gfx/frenesi_icon.png");
    	powImagenApareciendo2 = new ImagenApareciendo(530, (Configuracion.SCREEN_HEIGHT - powImage2.getHeight()) / 2, powImage2, 1000, 45f);
    	Image powImage3 = new Image("gfx/frenesi_icon.png");
    	powImagenApareciendo3 = new ImagenApareciendo(470, (Configuracion.SCREEN_HEIGHT - powImage3.getHeight()) / 2 + 70, powImage3, 1000, -30f);
    	Image powImage4 = new Image("gfx/frenesi_icon.png");
    	powImagenApareciendo4 = new ImagenApareciendo(500, (Configuracion.SCREEN_HEIGHT - powImage4.getHeight()) / 2 + 100, powImage4, 1000, 57f);

	}

	@Override
	protected void salir(GameContainer container, StateBasedGame game) throws SlickException {
		
    	SfxPlayer.unloadSounds(new byte[]{SfxPlayer.OUCH, SfxPlayer.YES, SfxPlayer.VIDA, SfxPlayer.FRENETICO_MUSICA, SfxPlayer.FRENETICO});
	}
	
    
    /**
     * Dibuja el HUD de las vidas del jugador
     * @param g
     */
    private void dibujarHudVidas(Graphics g) {
    	
    	for(int i=1; i<=vidasRestantes; i++) {
    		
    		vidaImage.draw((40 * - i) + Configuracion.SCREEN_WIDTH - 10, 14);
    	}
    }
    
    /**
     * Dibujo el HUD del frenesi del jugador
     * @param g
     */
    private void dibujarHudFrenesi(Graphics g) {
    	
    	if(powImagenApareciendo1 != null) {
            //renderizamos los golpes
        	powImagenApareciendo1.render(g);
        	powImagenApareciendo2.render(g);
        	powImagenApareciendo3.render(g);
        	powImagenApareciendo4.render(g);
    	}
    	
    	
    	frenesiImage.draw(Configuracion.SCREEN_WIDTH - 140, 44);
    	LittleSquareGame.angelCodeFont.drawString(Configuracion.SCREEN_WIDTH - 70, 50, " X " + frenesis);
    	
    }

    public void renderizar(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {

    	/**
    	 * PLAYING OR PAUSE
    	 */
		if(currentState == STATES.PLAYING_GAME_STATE || currentState == STATES.PAUSE_GAME_STATE) {
			
			//dibujamos el hud de las vidas
			dibujarHudVidas(g);
			
			//dibujamos el hud del frenesi
			dibujarHudFrenesi(g);

			//mostramos la cantidad de 
			//cuadrados amigos que fueron
			//agarrados
			LittleSquareGame.angelCodeFont.drawString(10, 5, "" + score);

		}
    	
		/**
		 * Manejador de cuadrados 
		 */
		manejadorCuadrados.render(gc, sbg, g);

		/**
		 * Si el jugador ha pausado el juego
		 * entonces mostramos el menu de 
		 * pausa
		 */
        if(currentState == STATES.PAUSE_GAME_STATE) {
        	
        	//renderizamos un fondo transparente
        	//para dar un efecto multiloco
    		g.setColor(grisTransparente);
    		g.fillRect(0, 0, Configuracion.SCREEN_WIDTH, Configuracion.SCREEN_HEIGHT);
        	
    		//renderizamos los botones
    		for(Boton boton : botones) {
    			boton.render(g);
    		}
        }
        
        /**
         * Si estamos en FRENESI
         */
        if(tiempoActualFrenesi > 0) {
        	
        	//ancho de la barra  
        	//de tiempo dinamica
        	int anchoBarraTiempo = (tiempoActualFrenesi * 360) / TIEMPO_TOTAL_FRENESI;
        	
        	if(barraTiempoFrenesiColor != null) {
        		
            	//cambiamos color de
            	//barra aleatoriamente
            	barraTiempoFrenesiColor.r = (float)Math.random();
            	barraTiempoFrenesiColor.g = (float)Math.random();
            	barraTiempoFrenesiColor.b = (float)Math.random();
            	g.setColor(barraTiempoFrenesiColor);
        	}
        	
    		//dibujamos la barra 
    		//multicolor de tiempo
        	g.fillRect(110, 10, anchoBarraTiempo, 10);
        	
        }
        
    	/**
    	 * GAME OVER
    	 */
    	if(currentState == STATES.GAME_OVER_STATE) {
    		
    		if(mostrarCartelGameOver) {
    			LittleSquareGame.angelCodeFont.drawString((Configuracion.SCREEN_WIDTH - anchoTextoGameOver) / 2, (Configuracion.SCREEN_HEIGHT - altoTextoGameOver) / 2, gameOverTxt);
    		}
    	}
    	
    	/**
    	 * HIGHSCORE
    	 */
    	dibujarHighscore(g);
    
    }
    
    /**
     * Dibuja los elementos pertenecientes
     * al estado de highscore
     * 
     * @param g
     */
    private void dibujarHighscore(Graphics g) {
    	
    	if(currentState == STATES.HIGHSCORE_STATE) {
    		
    		//cuadrado transparente de fondo
    		g.setColor(grisTransparente);
    		g.fillRoundRect((Configuracion.SCREEN_WIDTH - MENU_HIGHSCORE_WIDTH) / 2 , ((Configuracion.SCREEN_HEIGHT - MENU_HIGHSCORE_HEIGHT) / 2) + 30, MENU_HIGHSCORE_WIDTH, MENU_HIGHSCORE_HEIGHT, 1);
    		
    		//imagen del titulo de highscore
    		highscoreImage.draw(190, 50);

    		//dibujamos la imagen 
    		//del perfil del jugador
        	if(MainMenuState.imagenPerfilJugador != null) {
        		MainMenuState.imagenPerfilJugador.draw(130, 115);
        	}
    	}
    }
    
    
    @Override
    public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {

    	super.update(gc, sbg, delta);
        switch(currentState)
        {
            case START_GAME_STATE:
            	
            	//reiniciamos el score a cero
            	score = 0;
            	
            	//asignamos un frenesi
            	//desde el arranque
            	frenesis = 1;
            	
            	//reiniciamos las vidas
            	vidasRestantes = 3;
            	
            	//reiniciamos el tiempo actual 
            	//de la partida
            	tiempoTotalPartida = 0;
            	
            	//inicializamos los cuadrados
            	cuadradoJugador = new CuadradoJugador();
            	manejadorCuadrados.inicializarCuadrados(cuadradoJugador);
            	
                //para que no se arranque por error
                //cuando comienza el juego
                ultimoTouchDown = 2000;
                
            	//pasamos el estado jugando
                currentState = STATES.PLAYING_GAME_STATE;
                
                break;
            case PLAYING_GAME_STATE:
            	
            	//incrementamos el tiempo
            	//actual de partida 
            	tiempoTotalPartida += delta;
            	
                //renderizamos los golpes
                //TODO Moverlo al bloque de 
                //codigo del frenesi
            	powImagenApareciendo1.update(delta);
            	powImagenApareciendo2.update(delta);
            	powImagenApareciendo3.update(delta);
            	powImagenApareciendo4.update(delta);
            	
            	//maneja la salida 
            	//de los cuadrados
            	manejadorCuadrados.update(gc, sbg, delta);
            	
            	//procesamos la logica del frenesi
            	logicaFrenesi(delta);
            	
                break;
            case HIGHSCORE_STATE:
            	logicaHighscore(delta);
                break;
            case PAUSE_GAME_STATE:
        		//actualizamos la logica
        		//de los botones
        		for(Boton boton : botones) {
        			boton.update(delta);
        		}

                break;
            case GAME_OVER_STATE:
            	logicaGameOver(delta);
                break;
        }
    }
    
    /**
     * Logica ejecutada cuando el
     * juego ha finalizado y se 
     * desea mostrar el nuevo highscore
     * del jugador
     * 
     * @param delta
     */
    private void logicaHighscore(int delta) {
    	
    	
    }
    
    /**
     * Logica ejecutada cuando el 
     * juego ha finalizado
     * @param delta
     */
    private void logicaGameOver(int delta) {
    	
    	/**
    	 * Logica para mostrar 
    	 * cartel de game over
    	 */
    	if(gameOverTimer == null) {
    		
    		//inicializamos el timer para
    		//que muestre el cartel
    		gameOverTimer = new Timer(750);
    		mostrarCartelGameOver = true;
    		repeticionesGameOver = 4;
    	}
    	
    	//contabilizamos el tiempo
    	if(gameOverTimer.action(delta)) {
    		
    		//si estabamos mostrando el cartel
    		if(mostrarCartelGameOver) {
    			
    			//lo dejamos de mostrar
    			mostrarCartelGameOver = false;
    			
    			//cambiamos el timing durante
    			//el apagado del cartel
    			gameOverTimer.setDelay(350);
    			gameOverTimer.setCurrentTick(0);
    			
    			//decrementamos las repeticiones
    			repeticionesGameOver--;
    			
    		}
    		else {
    			
    			//lo dejamos de mostrar
    			mostrarCartelGameOver = true;
    			
    			//cambiamos el timing durante
    			//el apagado del cartel
    			gameOverTimer.setDelay(750);
    			gameOverTimer.setCurrentTick(0);
    			
    		}
    		
    		//terminamos las repeticiones
    		//del cartel de game over
    		if(repeticionesGameOver == 0) {
    			
    			gameOverTimer = null;
    			currentState = STATES.HIGHSCORE_STATE;

    		}
    	}
    }
    
    private void logicaFrenesi(int delta) {
    	
    	if(tiempoActualFrenesi > 0) {
    		
    		//decrementamos tiempo frenesi
    		tiempoActualFrenesi -= delta;
    		
    		//finaliza frenesi y
    		//normalizamos variable
    		if(tiempoActualFrenesi <= 0) {

    			//quitamos el frenesi
    			for(Cuadrado cuadrado : manejadorCuadrados.cuadrados) {
    				if(cuadrado instanceof CuadradoAmigo || cuadrado instanceof CuadradoEnemigo) {
    					cuadrado.estadoFrenetico = false;
    				}
    			}

    			//normalizamos variable
    			tiempoActualFrenesi = 0;
    		}
    		
    		//tiempos de golpes
    		if(timerFrenesiGolpe1.action(delta)) {
    			timerFrenesiGolpe1.setActive(false);
    			powImagenApareciendo1.aparecer = true;
    		}

    		if(timerFrenesiGolpe2.action(delta)) {
    			timerFrenesiGolpe2.setActive(false);
    			powImagenApareciendo2.aparecer = true;
    		}
    		
    		if(timerFrenesiGolpe3.action(delta)) {
    			timerFrenesiGolpe3.setActive(false);
    			powImagenApareciendo3.aparecer = true;
    		}

    		if(timerFrenesiGolpe4.action(delta)) {
    			timerFrenesiGolpe4.setActive(false);
    			powImagenApareciendo4.aparecer = true;
    		}    		
    	}
    }
    
    /**
     * Para PC
     */
    public void touchMovido(int x, int y) {

    	if(currentState == STATES.PLAYING_GAME_STATE) {

    		//cambia la posicion del jugador
    		cambiarPosicionCuadradoJugador(x, y);
    	}
    }
    
    /**
     * Para Android
     */
	@Override
	public void mousePressed(int button, int x, int y) {
		
    	if(currentState == STATES.PLAYING_GAME_STATE) {
    		
    		//cambia la posicion del jugador
    		cambiarPosicionCuadradoJugador(x, y);
    	}
	}
	
	@Override
	public void touchArriba(int x, int y) {
		
	}

	
	@Override
	public boolean keyTyped(char keycode) {

		//llamamos a super
		super.keyTyped(keycode);
		
		return true;
	}
	
	/**
	 * Cambia la posicion del cuadrado
	 * correspondiente al jugador
	 */
	private void cambiarPosicionCuadradoJugador(int x, int y) {
		
		/**
		 * Aceleramos el mouse durante el juego
		 * TODO Esto podria cambiarse dinamicamente
		 * para agregar un nuevo poder al juego
		 */
		x = (int)(x * Configuracion.VELOCIDAD_MOUSE);
		y = (int)(y * Configuracion.VELOCIDAD_MOUSE);
		
		cuadradoJugador.cambiarPosicionCuadradoJugador(x, y);
	}
	
	public static void incrementarVida() {
		vidasRestantes++;	
	}
	
	public static void incrementarFrenesi() {
		frenesis++;	
	}
	
	public static void decrementarVida() {
		
		vidasRestantes--;
		if(vidasRestantes == 0) {
			
			currentState = STATES.GAME_OVER_STATE;
		}
	}
	
	/**
	 * Evento generado por alguno de 
	 * los botones que componen este
	 * estado
	 */
	@Override
	public void eventoGenerado(UIEvent event) {
		
		if(event.generador == reiniciarJuegoBoton) {
			
			currentState = STATES.START_GAME_STATE;
			manejadorCuadrados.cuadrados.clear();
			littleSquareGame.enterState(LittleSquareGame.GAMEPLAYSTATE, new FadeOutTransition(), new FadeInTransition());
		}
		else if(event.generador == resumirBoton) {

			currentState = STATES.PLAYING_GAME_STATE;
		}
		else if(event.generador == salirBoton) {

			manejadorCuadrados.cuadrados.clear();
			currentState = STATES.START_GAME_STATE;
			littleSquareGame.enterState(LittleSquareGame.MAINMENUSTATE, new FadeOutTransition(), new FadeInTransition());
		}
	}
	
	private void iniciarFrenesi() {

		/**
		 * Solo si el jugador se encuentra jugando
		 */
		if(currentState == STATES.PLAYING_GAME_STATE) {
			
			//si todavia tenemos algun
			//frenesi disponible
			if(frenesis > 0 && tiempoActualFrenesi == 0) {
				
				//decrementamos un frenesi
				frenesis--;

				//nuevo tiempo de frenesi
				tiempoActualFrenesi = TIEMPO_TOTAL_FRENESI;
				
				//activamos los tiempos de los golpes
				timerFrenesiGolpe1.setActive(true);
				timerFrenesiGolpe1.setCurrentTick(0);
				timerFrenesiGolpe2.setActive(true);
				timerFrenesiGolpe2.setCurrentTick(0);
				timerFrenesiGolpe3.setActive(true);
				timerFrenesiGolpe3.setCurrentTick(0);
				timerFrenesiGolpe4.setActive(true);
				timerFrenesiGolpe4.setCurrentTick(0);
				
				//recorre la lista de cuadrados
				//para dejarlos freneticos
				for(Cuadrado cuadrado : manejadorCuadrados.cuadrados) {
					if(cuadrado instanceof CuadradoAmigo || cuadrado instanceof CuadradoEnemigo) {
						cuadrado.iniciarFrenesi(TIEMPO_TOTAL_FRENESI);
					}
				}
				
				//reproducimos un yessss
				SfxPlayer.reproducir(SfxPlayer.FRENETICO_MUSICA);
			}
		}
	}
	
	@Override
	public void dobleTouch() {

		//iniciamos el frenesi
		iniciarFrenesi();
	}
	
	@Override
	public boolean keyUp(int keyCode) {

		//HOME BUTTON
		if(keyCode == 82) {
			
        	if(currentState == STATES.PLAYING_GAME_STATE) {
        		
        		currentState = STATES.PAUSE_GAME_STATE;
        		
        	}
        	else if(currentState == STATES.PAUSE_GAME_STATE) {
        		
        		currentState = STATES.PLAYING_GAME_STATE;

        	}
		}
		
		return true;
	}
}