package org.pol.littlesquare.state;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;
import org.pol.littlesquare.LittleSquareGame;
import org.pol.littlesquare.configuracion.Configuracion;
import org.pol.littlesquare.dominio.Cuadrado;
import org.pol.littlesquare.dominio.CuadradoAmigo;
import org.pol.littlesquare.dominio.CuadradoEnemigo;
import org.pol.littlesquare.dominio.CuadradoFrenetico;
import org.pol.littlesquare.dominio.CuadradoJugador;
import org.pol.littlesquare.dominio.CuadradoVida;


public class AyudaState extends AbstractState {
	
    private int stateID = -1;
    
    private Cuadrado cuadradoEnemigo;
    private Cuadrado cuadradoAmigo;
    private Cuadrado cuadradoVida;
    private Cuadrado cuadradoFrenetico;
    private Cuadrado cuadradoJugador;
    
    //flag que indica el flujo 
    //de esta pantalla luego de
    //de que se sale de la misma
    public static boolean continuaHaciaJuego;
    
    //ayuda distribuida en lineas
    private String[] lineasAyudaEnemigo = {"Quita una vida."};
    private String[] lineasAyudaAmigo = {"Incrementa el score."};
    private String[] lineasAyudaVida = {"Incrementa una vida."};
    private String[] lineasAyudaFrenetico = {"Acumula un cuadrado Frenetico.", "Para usarlo hacer DOBLE CLICK."};
	
    public AyudaState(int stateID) {
        this.stateID = stateID;
     }
    
    @Override
    public int getID() {
        return stateID;
    }

    @Override
    public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {

    	super.update(gc, sbg, delta);
    	
		cuadradoEnemigo.update(gc, sbg, delta, null);
		cuadradoAmigo.update(gc, sbg, delta, null);
		cuadradoVida.update(gc, sbg, delta, null);
		cuadradoFrenetico.update(gc, sbg, delta, null);
		cuadradoJugador.update(gc, sbg, delta, null);
		
    }
    	

	@Override
	public void renderizar(GameContainer gameContainer, StateBasedGame sbg, Graphics g) throws SlickException {
		
		if(cuadradoEnemigo != null) {
			
			/**
			 * Dibujamos todos los cuadrados
			 */
			cuadradoJugador.render(gameContainer, sbg, g);
			LittleSquareGame.angelCodeFont.drawString(cuadradoJugador.x + (Cuadrado.ANCHO_ESTANDAR_JUGADOR / 2) - 15, cuadradoJugador.y + Cuadrado.ALTO_ESTANDAR_JUGADOR + 10, "TU");

			cuadradoEnemigo.render(gameContainer, sbg, g);
			LittleSquareGame.angelCodeFont.drawString(155, 90, "ENEMIGO");
			
			cuadradoAmigo.render(gameContainer, sbg, g);
			LittleSquareGame.angelCodeFont.drawString(440, 90, "AMIGO");
			
			cuadradoVida.render(gameContainer, sbg, g);
			LittleSquareGame.angelCodeFont.drawString(155, 160, "VIDA");

			cuadradoFrenetico.render(gameContainer, sbg, g);
			LittleSquareGame.angelCodeFont.drawString(440, 160, "FRENETICO");
			
			/**
			 * Dibujamos la ayuda del cuadrado
			 * que esta colisionando con el del
			 * jugador
			 */
			if(cuadradoJugador.colisiona(cuadradoEnemigo)) {
				for(int i=0; i<lineasAyudaEnemigo.length; i++) {
					LittleSquareGame.angelCodeFont.drawString(240, 300 + (i * 40), lineasAyudaEnemigo[i]);
				}
			}
			else if(cuadradoJugador.colisiona(cuadradoAmigo)) {
				for(int i=0; i<lineasAyudaAmigo.length; i++) {
					LittleSquareGame.angelCodeFont.drawString(200, 300 + (i * 40), lineasAyudaAmigo[i]);
				}
			}
			else if(cuadradoJugador.colisiona(cuadradoVida)) {
				for(int i=0; i<lineasAyudaVida.length; i++) {
					LittleSquareGame.angelCodeFont.drawString(200, 300 + (i * 40), lineasAyudaVida[i]);
				}
			}
			else if(cuadradoJugador.colisiona(cuadradoFrenetico)) {
				for(int i=0; i<lineasAyudaFrenetico.length; i++) {
					LittleSquareGame.angelCodeFont.drawString(120, 300 + (i * 40), lineasAyudaFrenetico[i]);
				}
			}
		}
	}

	@Override
	protected void entrar(GameContainer container, StateBasedGame game) throws SlickException {

		cuadradoEnemigo = new CuadradoEnemigo(100, 100);
		cuadradoAmigo = new CuadradoAmigo(380, 100);
		cuadradoVida = new CuadradoVida(100, 170);
		cuadradoFrenetico = new CuadradoFrenetico(380, 170);
		cuadradoJugador = new CuadradoJugador((Configuracion.SCREEN_WIDTH - Cuadrado.ANCHO_ESTANDAR_JUGADOR) / 2, 330);
		
	}

	@Override
	protected void salir(GameContainer container, StateBasedGame game) throws SlickException {

	}

	@Override
	public void touchMovido(int x, int y) {
	
		cuadradoJugador.x = x;
		cuadradoJugador.y = y;
		
	}
	
	@Override
	public void touchArriba(int x, int y) {
		
		
	}

	@Override
	public boolean touchDown(int arg0, int arg1, int arg2, int arg3) {
		
		if(continuaHaciaJuego) {
			littleSquareGame.enterState(LittleSquareGame.GAMEPLAYSTATE, new FadeOutTransition(), new FadeInTransition());	
		}
		else {
			littleSquareGame.enterState(LittleSquareGame.MAINMENUSTATE, new FadeOutTransition(), new FadeInTransition());
		}
		
		return true;
	}
	
	@Override
	public void dobleTouch() {

	}
}