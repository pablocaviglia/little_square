package org.pol.littlesquare.state;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.pol.littlesquare.LittleSquareGame;
import org.pol.littlesquare.configuracion.Configuracion;
import org.pol.littlesquare.ui.event.EventListener;
import org.pol.littlesquare.ui.event.GestosTouchListener;
import org.pol.littlesquare.ui.event.UIEvent;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;

public abstract class AbstractState extends BasicGameState implements InputProcessor, EventListener, GestosTouchListener {

	/**
	 * Muestra la imagen del cursor
	 * 
	 * -> Es necesario para que se vea el cursor, que se 
	 * -> llame desde el render de la implementacion de 
	 * -> esta clase al render de esta clase (abstracta)
	 * -> como asi tambien el metodo touchMoved
	 */
	
    /**
     * Usado para calcular el
     * double touch o click en
     * pantalla
     */
    protected long ultimoTouchDown;
	
	protected LittleSquareGame littleSquareGame;
	
	//color de fondo del juego
	private Color colorFondo = Color.white;
	
	@Override
	public int getID() {
		return 0;
	}

	@Override
	public void init(GameContainer gameContainer, StateBasedGame sbg) throws SlickException {
		
	}

	public abstract void renderizar(GameContainer gameContainer, StateBasedGame sbg, Graphics g) throws SlickException;
	
	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		
		//pintamos el fondo
		g.setColor(colorFondo);
		g.fillRect(0, 0, gc.getWidth(), gc.getHeight());
		
		//renderizamos la implementacion
		renderizar(gc, sbg, g);
		
	}

	@Override
	public void update(GameContainer gameContainer, StateBasedGame sbg, int delta) throws SlickException {
		
		//incrementamos el touch up
		ultimoTouchDown += delta;
		if(ultimoTouchDown <= 0) {
			ultimoTouchDown = 0;
		}
	}
	
    @Override
    public void enter(GameContainer container, StateBasedGame game) throws SlickException {
    	
    	//seteamos el nuevo procesador
    	//de entrada en GDX mismo
    	Gdx.input.setInputProcessor(this);
    	
    	//logica de cada clase de estado
    	entrar(container, game);
    	
    	//referenciamos al contenedor
    	littleSquareGame = (LittleSquareGame)game;
    	
    }
    
    @Override
    public void leave(GameContainer container, StateBasedGame game) throws SlickException {
    	
    	//seteamos el nuevo procesador
    	//de entrada en GDX mismo
    	Gdx.input.setInputProcessor(null);
    	
    	//logica de cada clase de estado
    	salir(container, game);
    	
    }

    /**
     * Implementado por las clases
     * de estado en si para indicar
     * la ejecucion de algun tipo de 
     * accion en la entrada al estado
     * @param container
     * @param game
     */
    protected abstract void entrar(GameContainer container, StateBasedGame game) throws SlickException;
    
    /**
     * Implementado por las clases
     * de estado en si para indicar
     * la ejecucion de algun tipo de 
     * accion en la salide del estado
     * @param container
     * @param game
     */
    protected abstract void salir(GameContainer container, StateBasedGame game) throws SlickException;
    
	public abstract void touchMovido(int x, int y);
	public abstract void touchArriba(int x, int y);
	
	@Override
	public boolean touchMoved(int x, int y) {
	
		touchMovido(recalcularX(x), recalcularY(y));
		return true;
	}
	
	@Override
	public boolean touchDragged(int x, int y, int nose) {
		
		touchMovido(recalcularX(x), recalcularY(y));
		return true;
	}
	
	private int recalcularX(int x) {
		
		float porcentajeIncrementoX = ((float)Configuracion.SCREEN_WIDTH) / ((float)Configuracion.SCREEN_WIDTH_REAL_ANDROID);
		int nuevoX = (int)(x * porcentajeIncrementoX);

		return nuevoX;
	}
	
	private int recalcularY(int y) {
		
		float porcentajeIncrementoY = ((float)Configuracion.SCREEN_HEIGHT) / ((float)Configuracion.SCREEN_HEIGHT_REAL_ANDROID);
		int nuevoY = (int)(y * porcentajeIncrementoY);

		return nuevoY;
	}
	
	@Override
	public boolean touchUp(int x, int y, int arg2, int arg3) {
		
		touchArriba(recalcularX(x), recalcularY(y));
		return true;
	}
	
	@Override
	public boolean touchDown(int arg0, int arg1, int arg2, int arg3) {
		
    	//si el ultimo evento touchup
    	//fue generado hace menos de 
    	//determinado tiempo, entonces
    	//consideramos un doble click
		if(ultimoTouchDown < 500) {
			dobleTouch();
		}
		
		//reseteamos valor
		ultimoTouchDown = 0;

		return true;
	}
	
	public boolean scrolled(int arg0) {
		return false;
	}
	
	public boolean keyUp(int keyCode) {
		
		return true;
	}
	
	public boolean keyTyped(char arg0) {
		
		return true;
	}
	
	public boolean keyDown(int arg0) {
		return false;
	}
	
	public void eventoGenerado(UIEvent event) {
		
	}
	
	@Override
	public boolean isAcceptingInput() {
		return true;
	}
}