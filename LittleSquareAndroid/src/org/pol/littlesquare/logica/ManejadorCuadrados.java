package org.pol.littlesquare.logica;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import org.pol.littlesquare.dominio.Cuadrado;
import org.pol.littlesquare.dominio.CuadradoAmigo;
import org.pol.littlesquare.dominio.CuadradoEnemigo;
import org.pol.littlesquare.dominio.CuadradoFrenetico;
import org.pol.littlesquare.dominio.CuadradoJugador;
import org.pol.littlesquare.dominio.CuadradoVida;
import org.pol.littlesquare.state.GameplayState;

public class ManejadorCuadrados {

	/**
	 * Tiempo que debe de 
	 * pasar para que salga
	 * un nuevo cuadrado. 
	 * Este tiempo va cambiando
	 * a lo largo del juego 
	 */
	private long TIEMPO_SALIDA_CUADRADO = 1500;
	
	/**
	 * Milisegundos que pasaron
	 * desde que se libero el
	 * ultimo cuadrado
	 */
	private long salidaUltimoCuadrado;
	
	/**
	 * Cantidad de cuadrados que salieron
	 */
	public int cantidadCuadradosSalidos = 0;
	public int cantidadCuadradosAmigosSalidos;
	public int cantidadCuadradosEnemigosSalidos;
	
	/**
	 * Lista de cuadrados que
	 * componen el juego
	 */
	public List<Cuadrado> cuadrados;

	/**
	 * Lista de cuadrados a agregar y remover.
	 * Se usan colecciones aparte para funcionar
	 * de una manera sincronica durante el manejo
	 * de elementos en las colecciones
	 */
	public List<Cuadrado> cuadradosAgregar;
	public List<Cuadrado> cuadradosRemover;

	/**
	 * El siguiente array define en que salida
	 * de cuadrados sale cada nuevo cuadrado 
	 * frenetico
	 */
	private int[] numerosCuadradosSalidosAparicionFrenetico = {40, 100, 150, 200, 300, 400, 500, 600, 700, 800, 900, 1000};
	
	/**
	 * El siguiente array define en que salida
	 * de cuadrados sale cada nuevo cuadrado 
	 * de vida
	 */
//	private int[] numerosCuadradosSalidosAparicionVida = {60, 130, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100};
	private int[] numerosCuadradosSalidosAparicionVida = {2, 10, 15, 20, 25, 30, 35, 40, 45, 50};
	
    public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
    	
    	//ya paso el tiempo necesario para
    	//que se libere un nuevo cuadrado
    	if(salidaUltimoCuadrado <= 0) {
    		
    		//configuramos el nuevo tiempo
    		//de salida de cuadrados
    		salidaUltimoCuadrado = TIEMPO_SALIDA_CUADRADO;
    		
    		//liberamos un nuevo cuadrado
    		liberarCuadrado();
    		
    	}
    	else {
    		//decrementamos el tiempo de 
    		//salida del proximo cuadrado
    		salidaUltimoCuadrado -= delta;
    	}
    	
    	//en caso de que hayan cuadrados
    	//por agregar, lo hacemos
    	if(cuadradosAgregar.size() > 0) {
    		cuadrados.addAll(cuadradosAgregar);
    		cuadradosAgregar.clear();
    	}

    	//en caso de que hayan cuadrados
    	//por remover, lo hacemos
    	if(cuadradosRemover.size() > 0) {
    		cuadrados.removeAll(cuadradosRemover);
    		cuadradosRemover.clear();
    	}
    	
    	//ejecutamos la logica 
    	//de cada cuadrado
    	for(Cuadrado cuadrado : cuadrados) {
    		
    		cuadrado.update(gc, sbg, delta, cuadrados);
    		
    		//si el flag de removido esta
    		//seteado, lo removemos 
    		if(cuadrado.remover) {
    			removerCuadrado(cuadrado);
    		}
    	}
    }
    
    public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
    	
    	if(cuadrados != null) {
    		
            /**
        	 * Renderizamos los cuadrados
        	 */
        	for(Cuadrado cuadradoActual : cuadrados) {
        		
        		//renderizamos el cuadrado actual
        		cuadradoActual.render(gc, sbg, g);
        	}
    	}
    }

    
    /**
     * Libera un cuadrado amigo o enemigo
     * y configura la dificultad en base
     * a la cantidad de cuadrados que ya
     * salieron
     */
    private void liberarCuadrado() {
    	
    	//calculamos en base a cuantos
    	//cuadrados van saliendo la
    	//probabilidad de que salga un
    	//cuadrado amigo
    	int probabilidadSalidaAmigo = 100;
    	
    	//dificultad asignada
    	Cuadrado.DIFICULTAD dificultad = null;
    	
    	//en base a los que salieron evaluamos
    	//dificultad y probabilidad de salida
    	//de cuadrado amigo
    	if(cantidadCuadradosSalidos <= 10) {

    		dificultad = Cuadrado.DIFICULTAD.FACIL;
    		TIEMPO_SALIDA_CUADRADO = 500;
    	}
    	else if(cantidadCuadradosSalidos > 10 && cantidadCuadradosSalidos <= 40) {

    		probabilidadSalidaAmigo = 80;
    		dificultad = Cuadrado.DIFICULTAD.FACIL;
    		TIEMPO_SALIDA_CUADRADO = 500;
    	}
    	else if(cantidadCuadradosSalidos > 40 && cantidadCuadradosSalidos <= 70) {
    		
    		probabilidadSalidaAmigo = 70;
    		dificultad = Cuadrado.DIFICULTAD.MEDIANO;
    		TIEMPO_SALIDA_CUADRADO = 500;
    	}
    	else if(cantidadCuadradosSalidos > 70 && cantidadCuadradosSalidos <= 120) {
    		
    		probabilidadSalidaAmigo = 60;
    		dificultad = Cuadrado.DIFICULTAD.MEDIANO;
    		TIEMPO_SALIDA_CUADRADO = 400;
    	}
    	else if(cantidadCuadradosSalidos > 120 && cantidadCuadradosSalidos <= 180) {
    		
    		probabilidadSalidaAmigo = 60;
    		dificultad = Cuadrado.DIFICULTAD.MEDIANO;
    		TIEMPO_SALIDA_CUADRADO = 300;
    	}
    	else if(cantidadCuadradosSalidos > 180 && cantidadCuadradosSalidos <= 250) {
    		
    		probabilidadSalidaAmigo = 50;
    		dificultad = Cuadrado.DIFICULTAD.DIFICIL;
    		TIEMPO_SALIDA_CUADRADO = 300;
    	}
    	else if(cantidadCuadradosSalidos > 250 && cantidadCuadradosSalidos <= 300) {
    		
    		probabilidadSalidaAmigo = 50;
    		dificultad = Cuadrado.DIFICULTAD.MUY_DIFICIL;
    		TIEMPO_SALIDA_CUADRADO = 300;
    	}
    	else if(cantidadCuadradosSalidos > 300) {
    		
    		probabilidadSalidaAmigo = 50;
    		dificultad = Cuadrado.DIFICULTAD.IMPOSIBLE;
    		TIEMPO_SALIDA_CUADRADO = 180;
    	}
    	
    	/**
    	 * Flag que indica si sale 
    	 * amigo o enemigo
    	 */
    	boolean saleAmigo = false;
    	
    	/**
    	 * En base a la probabilidad de
    	 * salida de cuadrados amigos 
    	 * creamos un numero aleatorio
    	 */
    	float probabilidadEfectiva = (float)Math.random() * 100f;
    	if(probabilidadEfectiva < probabilidadSalidaAmigo) {
    		saleAmigo = true;
    	}
    	
    	//referencia al cuadrado que va a salir
    	Cuadrado cuadradoSalidor = null;
    	if(saleAmigo) {
    		cuadradoSalidor = new CuadradoAmigo(Cuadrado.ANCHO_ESTANDAR_AMIGO, Cuadrado.ALTO_ESTANDAR_AMIGO);
    		cantidadCuadradosAmigosSalidos++;
    	}
    	else {
    		cuadradoSalidor = new CuadradoEnemigo(Cuadrado.ANCHO_ESTANDAR_ENEMIGO, Cuadrado.ALTO_ESTANDAR_ENEMIGO);
    		cantidadCuadradosEnemigosSalidos++;
    	}
    	
    	/**
    	 * Chequeamos si el cuadrado debe
    	 * de salir en modo frenesi
    	 */
    	if(GameplayState.tiempoActualFrenesi > 0) {
        	if(cuadradoSalidor instanceof CuadradoAmigo || cuadradoSalidor instanceof CuadradoEnemigo) {
        		cuadradoSalidor.iniciarFrenesi(GameplayState.tiempoActualFrenesi);
        	}
    	}
    	
    	/**
    	 * Agregamos el cuadrado a la lista 
    	 * de cuadrados del juego
    	 */
    	agregarCuadrado(cuadradoSalidor);
    	
    	//iniciamos su recorrido
    	cuadradoSalidor.crearRecorrido(dificultad);
    	
    	//incrementamos en uno la
    	//cantidad de cuadrados salidos
    	cantidadCuadradosSalidos++;
    	
    	/**
    	 * Generacion de cuadrados de vida
    	 */
    	for(int numeroActual : numerosCuadradosSalidosAparicionVida) {
    		
    		//si el numero de salida de cuadrados
    		//actual coincide con la lista de salida
    		//de cuadrados de vida, entonces liberamos uno
    		if(cantidadCuadradosSalidos == numeroActual) {
    			
        		//si salieron la cantidad de
        		//cuadrados indicados entonces
        		//generamos un nuevo cuadrado 
        		//de vida
        		CuadradoVida cuadradoVida = new CuadradoVida(-Cuadrado.ANCHO_ESTANDAR_VIDA, -Cuadrado.ALTO_ESTANDAR_VIDA);
            	agregarCuadrado(cuadradoVida);
            	cuadradoVida.crearRecorrido(dificultad);
        		
    		}
    	}
    	
    	/**
    	 * Generacion de cuadrados freneticos
    	 */
    	for(int numeroActual : numerosCuadradosSalidosAparicionFrenetico) {
    		
    		//si el numero de salida de cuadrados
    		//actual coincide con la lista de salida
    		//de cuadrados freneticos, entonces 
    		//liberamos uno
    		if(cantidadCuadradosSalidos == numeroActual) {
    			
        		//si salieron la cantidad de
        		//cuadrados indicados entonces
        		//generamos un nuevo cuadrado 
        		//frenetico
        		CuadradoFrenetico cuadradoFrenetico = new CuadradoFrenetico(-Cuadrado.ANCHO_ESTANDAR_FRENETICO, -Cuadrado.ALTO_ESTANDAR_FRENETICO);
            	agregarCuadrado(cuadradoFrenetico);
            	cuadradoFrenetico.crearRecorrido(dificultad);
        		
    		}
    	}
    }
    
    /**
     * Agrega un cuadrado, el cual va a ser
     * efectivamente agregado durante el
     * proximo loop del juego
     * @param cuadrado
     */
    public void agregarCuadrado(Cuadrado cuadrado) {
    	
    	cuadradosAgregar.add(cuadrado);
    }
    
    /**
     * Remueve un cuadrado, el cual va a ser
     * efectivamente removido durante el
     * proximo loop del juego
     * @param cuadrado
     */
    public void removerCuadrado(Cuadrado cuadrado) {
    	
    	cuadradosRemover.add(cuadrado);
    }
    
    /**
     * Inicializa las colecciones 
     * de cuadrados y recibe la
     * referencia al cuadrado del
     * jugador
     * 
     * @param cuadradoJugador
     */
    public void inicializarCuadrados(CuadradoJugador cuadradoJugador) {

    	cuadrados = new ArrayList<Cuadrado>();
    	cuadradosAgregar = new ArrayList<Cuadrado>();
    	cuadradosRemover = new ArrayList<Cuadrado>();
    	
    	cantidadCuadradosSalidos = 0;
    	cantidadCuadradosAmigosSalidos = 0;
    	cantidadCuadradosEnemigosSalidos = 0;
    	
    	/**
    	 * Damos un grado de aleatoriedad
    	 * al momento de generacion de los
    	 * cuadrados freneticos 
    	 */
    	for(int i=0; i<numerosCuadradosSalidosAparicionFrenetico.length; i++) {
    		
    		//obtenemos el numero actual
    		int numeroActual = numerosCuadradosSalidosAparicionFrenetico[i];

    		//hacemos que el numero actual presente 
    		//cierto grado de aleatoriedad
    		numeroActual += (Math.random() * 15d);
    		
    		//volvemos el numero al array
    		numerosCuadradosSalidosAparicionFrenetico[i] = numeroActual;
    		
    	}
    	
    	/**
    	 * Damos un grado de aleatoriedad
    	 * al momento de generacion de los
    	 * cuadrados de vida 
    	 */
    	for(int i=0; i<numerosCuadradosSalidosAparicionVida.length; i++) {
    		
    		//obtenemos el numero actual
    		int numeroActual = numerosCuadradosSalidosAparicionVida[i];

    		//hacemos que el numero actual presente 
    		//cierto grado de aleatoriedad
    		numeroActual += (Math.random() * 15d);
    		
    		//volvemos el numero al array
    		numerosCuadradosSalidosAparicionVida[i] = numeroActual;
    		
    	}
    	
    	//quitamos todos los cuadrados
    	cuadrados.clear();
    	cuadradosAgregar.clear();
    	cuadradosRemover.clear();
    	
    	if(cuadradoJugador != null) {
    		
        	//agregamos el cuadrado del jugador
        	//a la lista comun de cuadrados
        	agregarCuadrado(cuadradoJugador);
    	}
    }
}