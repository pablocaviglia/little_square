package org.pol.littlesquare;

import org.newdawn.slick.AngelCodeFont;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import org.pol.littlesquare.iface.IActivityRequestHandler;
import org.pol.littlesquare.persistencia.Persistencia;
import org.pol.littlesquare.state.AyudaState;
import org.pol.littlesquare.state.GameplayState;
import org.pol.littlesquare.state.MainMenuState;

public class LittleSquareGame extends StateBasedGame {

    public static final int MAINMENUSTATE          = 0;
    public static final int GAMEPLAYSTATE          = 1;
    public static final int AYUDASTATE             = 2;

	public static AngelCodeFont angelCodeFont;
	
	//manejador de request para android
	private static IActivityRequestHandler myRequestHandler;
	
	/**
	 * Cursor del juego
	 */
	public static Image cursorImage;

	/**
	 * Persistencia local del juego.
	 * Para las partidas que son locales
	 * y no remotas
	 */
	public static Persistencia persistencia;
	
	public LittleSquareGame() {
		
		super("Little Square");
		
        this.addState(new MainMenuState(MAINMENUSTATE));
        this.addState(new GameplayState(GAMEPLAYSTATE));
        this.addState(new AyudaState(AYUDASTATE));
        
        //entramos al primer 
        //estado del juego
        enterState(MAINMENUSTATE);

	}
	
	public void setHandler(IActivityRequestHandler handler) {
		
		//handler de android
		LittleSquareGame.myRequestHandler = handler;
	}

	@Override
	public void initStatesList(GameContainer container) throws SlickException {

		getState(GAMEPLAYSTATE).init(container, this);
		getState(MAINMENUSTATE).init(container, this);
		getState(AYUDASTATE).init(container, this);
		
		angelCodeFont = new AngelCodeFont("fuente_1.fnt", "fuente_1.png");
		
		//sincronizamos el video
		container.setVSync(true);
		
		//no mostramos los FPS
        container.setShowFPS(false);
        container.setTargetFrameRate(30);
        
		//Cargamos el cursor
		cursorImage = new Image("gfx/cursor.png");

		//instanciamos la clase que se
		//encarga de persistir los datos
		//del juego para siguientes usos
		persistencia = new Persistencia();
	
	}
	
	public static void mostrarAds(boolean mostrar) {
		
		if(myRequestHandler != null) {
			if(mostrar) {
				myRequestHandler.showAds(true);
			}
			else {
				myRequestHandler.showAds(false);
			}
		}
	}
}