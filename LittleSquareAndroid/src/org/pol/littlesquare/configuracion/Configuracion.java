package org.pol.littlesquare.configuracion;

public class Configuracion {

	public static int SCREEN_WIDTH = 640;
	public static int SCREEN_HEIGHT = 480;
	
	public static int SCREEN_WIDTH_REAL_ANDROID;
	public static int SCREEN_HEIGHT_REAL_ANDROID;
	public static final String ADMOB_CODE = "a15124d7010c077";
	public static boolean AUDIO_HABILITADO = true;
	
	/**
	 * Velocidades prefijadas para el 
	 * control del mouse durante el juego
	 */
	private static float VELOCIDAD_MOUSE_LENTA = 0.75f;
	private static float VELOCIDAD_MOUSE_NORMAL = 1f;
	private static float VELOCIDAD_MOUSE_RAPIDA = 1.75f;

	/**
	 * Velocidad de mouse asignada al usuario
	 */
	public static float VELOCIDAD_MOUSE = VELOCIDAD_MOUSE_NORMAL;
	
	
}
