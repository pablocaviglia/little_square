package org.pol.littlesquare.dominio;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class CuadradoEnemigo extends Cuadrado {

	/**
	 * Color original de los cuadrados enemigos
	 */
	private static final Color COLOR_ORIGINAL = Color.red;    
    
    /**
     * Cantidad de cuadrados
     * enemigos agarrados
     */
    public static int agarrados;
    
	public CuadradoEnemigo(int x, int y) {
		
		//color del cuadrado
		color = new Color(COLOR_ORIGINAL.r, COLOR_ORIGINAL.g, COLOR_ORIGINAL.b);
		
		//configuramos el ancho y 
		//alto del cuadrado
		width = ANCHO_ESTANDAR_AMIGO;
		height = ALTO_ESTANDAR_AMIGO;
		
		//seteamos la posicion
		this.x = x;
		this.y = y;

	}
	
	@Override
	public void renderizar(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		
		
	}
	
	@Override
	public void actualizar(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
		
		
	}

	@Override
	protected void comienzaColisionCuadradoEvent(Cuadrado cuadrado) {
		
		if(cuadrado instanceof CuadradoJugador && !finalizado) {

			//asi no entramos de nuevo 
			//a este evento
			finalizado = true;

			//enemigos tocados
			agarrados++;
			
			//si el cuadrado esta frenetico
			//lo achicamos cuando se agarra
			if(estadoFrenetico) {
				iniciarEfectoAchicado(600);
			}
			
		}
	}
	
	protected void volverColorOriginal() {
		
		color.r = COLOR_ORIGINAL.r;
		color.g = COLOR_ORIGINAL.g;
		color.b = COLOR_ORIGINAL.b;
		color.a = COLOR_ORIGINAL.a;
	}
	
	protected void finalizaColisionCuadradoEvent(Cuadrado cuadrado) {
		
	}
}