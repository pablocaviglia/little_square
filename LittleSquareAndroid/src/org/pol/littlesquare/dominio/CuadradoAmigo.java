package org.pol.littlesquare.dominio;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class CuadradoAmigo extends Cuadrado {
	
	/**
	 * Color original de los cuadrados amigos
	 */
	private static final Color COLOR_ORIGINAL = Color.gray;    
	
    /**
     * Cantidad de cuadrados
     * amigos agarrados
     */
    public static int agarrados;
    
	public CuadradoAmigo(int x, int y) {
		
		//color del cuadrado
		color = new Color(COLOR_ORIGINAL.r, COLOR_ORIGINAL.g, COLOR_ORIGINAL.b);
		
		//configuramos el ancho y 
		//alto del cuadrado
		width = ANCHO_ESTANDAR_AMIGO;
		height = ALTO_ESTANDAR_AMIGO;
		
		//seteamos la posicion
		this.x = x;
		this.y = y;

	}
	
	@Override
	public void renderizar(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		
		
	}
	
	@Override
	public void actualizar(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
		
	}
	
	@Override
	protected void comienzaColisionCuadradoEvent(Cuadrado cuadrado) {
		
		/**
		 * Si colisiona con el jugador
		 * entonces comenzamos la 
		 * animacion de desaparicion
		 * del cuadrado, y luego la
		 * reproduccion de un sonido
		 */
		if(cuadrado instanceof CuadradoJugador && !finalizado) {
			
			//comenzamos el efecto de achicado
			iniciarEfectoAchicado(600);
			
			//asi no entramos de nuevo 
			//a este evento
			finalizado = true;
			
			//incrementamos cantidad de
			//cuadrados amigos agarrados
			agarrados++;
			
		}
	}
	
	protected void volverColorOriginal() {
		
		color.r = COLOR_ORIGINAL.r;
		color.g = COLOR_ORIGINAL.g;
		color.b = COLOR_ORIGINAL.b;
		color.a = COLOR_ORIGINAL.a;
	}
	
	protected void finalizaColisionCuadradoEvent(Cuadrado cuadrado) {
		
	}
}