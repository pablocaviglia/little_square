package org.pol.littlesquare.dominio;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import org.pol.littlesquare.state.MainMenuState;

public class CuadradoVida extends Cuadrado {
	
	/**
	 * Cuando el cuadrado es comido
	 * este estado es cambiado antes
	 * de destruirse el cuadrado
	 */
	public boolean finalizado;
	
	/**
	 * Tiempo restante de efecto achicado
	 */
	private int tiempoRestanteAchicadoImagen;
	private int tiempoConfiguradoAchicadoImagen;
	private float escalaImagenPerfilJugador = 1f;
	
    /**
     * Cantidad de cuadrados
     * amigos agarrados
     */
    public static int agarrados;
    
	public CuadradoVida(int x, int y) {
		
		//color del cuadrado
		color = new Color(0f, 0f, 0f);
		
		//configuramos el ancho y 
		//alto del cuadrado
		width = ANCHO_ESTANDAR_VIDA;
		height = ALTO_ESTANDAR_VIDA;
		
		//seteamos la posicion
		this.x = x;
		this.y = y;

	}
	
	@Override
	public void renderizar(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		
		//si la imagen del perfil esta
		//presente, la ponemos como 
		//imagen del cuadrado vida 
		if(MainMenuState.imagenPerfilJugadorVida != null) {
			renderizar = false;
			MainMenuState.imagenPerfilJugadorVida.draw(x, y, escalaImagenPerfilJugador);
		}
	}
	
	@Override
	public void actualizar(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {

		//solo si no esta presente la
		//imagen del perfil del jugador
		//se despliega el estado multicolor
		if(MainMenuState.imagenPerfilJugadorVida == null) {
			color.r = (float)Math.random();
			color.g = (float)Math.random();
			color.b = (float)Math.random();
		}
		else {
			
			MainMenuState.imagenPerfilJugadorVida.draw(x, y, escalaImagenPerfilJugador);
		}
		
		//si todavia queda tiempo achicado
		if(tiempoRestanteAchicadoImagen > 0) {
			
			//decrementamos el tiempo
			tiempoRestanteAchicadoImagen -= delta;

			//cambiamos la escala de la imagen
			//con respecto al tiempo
			escalaImagenPerfilJugador = (float)(((float)tiempoRestanteAchicadoImagen * 1f) / (float)tiempoConfiguradoAchicadoImagen);
			
			if(tiempoRestanteAchicadoImagen <= 0) {
				
				//normalizamos el tiempo para 
				//que no quede menor que cero
				tiempoRestanteAchicadoImagen = 0;
				
				//removemos el cuadrado
				remover = true;

			}
		}
	}
	
	@Override
	protected void comienzaColisionCuadradoEvent(Cuadrado cuadrado) {
		
		/**
		 * Si colisiona con el jugador
		 * entonces comenzamos la 
		 * animacion de desaparicion
		 * del cuadrado, y luego la
		 * reproduccion de un sonido
		 */
		if(cuadrado instanceof CuadradoJugador && !finalizado) {
			
			if(MainMenuState.imagenPerfilJugadorVida == null) {
				//comenzamos el efecto de achicado
				iniciarEfectoAchicado(600);
			}
			else {
				iniciarEfectoAchicadoImagenPerfilJugador(600);
			}
			
			//asi no entramos de nuevo 
			//a este evento
			finalizado = true;
			
			//incrementamos cantidad de
			//cuadrados amigos agarrados
			agarrados++;
			
		}
	}
	
	private void iniciarEfectoAchicadoImagenPerfilJugador(int duracion) {
		
		tiempoConfiguradoAchicadoImagen = duracion;
		tiempoRestanteAchicadoImagen = duracion;
		
	}

	@Override
	protected void volverColorOriginal() {
		
	}
	
	protected void finalizaColisionCuadradoEvent(Cuadrado cuadrado) {
		
		
	}
}