package org.pol.littlesquare.dominio;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Curve;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;
import org.pol.littlesquare.configuracion.Configuracion;
import org.pol.littlesquare.state.GameplayState;


public abstract class Cuadrado {

	public int x;
	public int y;
	public int width;
	public int height;
	public Color color;
	
	/**
	 * Cuando el cuadrado es comido
	 * este estado es cambiado antes
	 * de destruirse el cuadrado
	 */
	public boolean finalizado;
	
	/**
	 * Declara el estado frenetico del cuadrado
	 */
	public boolean estadoFrenetico;
	
	/**
	 * Tiempo restante de efecto multicolor
	 */
	protected int tiempoRestanteMulticolor;
	
	/**
	 * Tiempo restante de efecto achicado
	 */
	protected int tiempoRestanteAchicado;
	protected int tiempoConfiguradoAchicado;
	protected int anchoOriginalAchicado;
	protected int altoOriginalAchicado;
	
	/**
	 * Variables para el efecto de 
	 * achicado y agrandado
	 */
	protected boolean achicadoAgrandadoHabilitado;
	protected int tiempoActualAchicadoAgrandado;
	protected int tiempoConfiguradoAchicadoAgrandado;
	protected boolean efectoAchicadoAgrandadoEstado;

	/**
	 * Este flag indica si el cuadrado
	 * debe de ser removido de la lista
	 * del manejador de cuadrados
	 */
	public boolean remover;
	
	public List<Cuadrado> cuadradoColisionList = new ArrayList<Cuadrado>();
	
    /**
     * DIFICULTAD_FACIL 					Bastante tiempo y recorrido lineal
     * DIFICULTAD_RELATIVAMENTE_FACIL		Algo de tiempo y recorrido lineal
     * DIFICULTAD_MEDIANO					Algo de tiempo y recorrido no lineal sencillo
     * DIFICULTAD_DIFICIL					Poco tiempo y recorrido no lineal algo complejo
     * DIFICULTAD_MUY_DIFICIL				Poco tiempo y recorrido no lineal complejo
     * DIFICULTAD_IMPOSIBLE					Muy poco tiempo y recorrido no lineal muy complejo
     */
	/**
	 * Posibles estados durante el juego
	 */
	public static enum DIFICULTAD {
		FACIL, RELATIVAMENTE_FACIL, MEDIANO, DIFICIL, MUY_DIFICIL, IMPOSIBLE
    }
	
	/**
	 * Tiempo total del recorrido
	 */
	private long TIEMPO_MAXIMO_RECORRIDO;
	
	/**
	 * Tiempo actual del recorrido
	 */
	private long tiempoActualRecorrido;
	
	/**
	 * Recorrido del cuadrado representado
	 * a traves de una curva de Bezier
	 */
	private Curve recorrido;
	
    /**
     * Helper usado para las colisiones
     */
    public Rectangle collisionHelper = new Rectangle(0,0,0,0);
	
	/**
	 * Area estandar que ocupa el
	 * cuadrado del jugador
	 */
	public static int ANCHO_ESTANDAR_JUGADOR = 60;
	public static int ALTO_ESTANDAR_JUGADOR = 60;
	
	/**
	 * Area estandar que ocupan
	 * los cuadrados amigos
	 */
	public static int ANCHO_ESTANDAR_AMIGO = 30;
	public static int ALTO_ESTANDAR_AMIGO = 30;
	
	/**
	 * Area estandar que ocupan
	 * los cuadrados amigos
	 */
	public static int ANCHO_ESTANDAR_VIDA = 45;
	public static int ALTO_ESTANDAR_VIDA = 45;
	
	/**
	 * Area estandar que ocupan
	 * los cuadrados enemigos
	 */
	public static int ANCHO_ESTANDAR_ENEMIGO = 40;
	public static int ALTO_ESTANDAR_ENEMIGO = 40;
	
	/**
	 * Area estandar que ocupan
	 * los cuadrados freneticos
	 */
	public static int ANCHO_ESTANDAR_FRENETICO = 50;
	public static int ALTO_ESTANDAR_FRENETICO = 50;
	
	
	public static GameplayState gameplayState;
	
	/**
	 * Por defecto se renderiza el 
	 * cuadrado de color 
	 */
	protected boolean renderizar = true; 
	
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		
		if(renderizar) {
			//pintamos el cuadrado
			g.setColor(color);
			g.fillRect(x, y, width, height);
		}
		
		renderizar(gc, sbg, g);
		
	}
	
    public abstract void renderizar(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException;

    protected abstract void finalizaColisionCuadradoEvent(Cuadrado cuadrado);
    
    /**
     * Logica del cuadrado
     * 
     * @param gc
     * @param sbg
     * @param delta
     * @throws SlickException
     */
    public void update(GameContainer gc, StateBasedGame sbg, int delta, List<Cuadrado> cuadrados) throws SlickException {

    	//referencia a la lista de todos los cuadrados
    	procesarColision(cuadrados);
    	
    	//procesa el recorrido del cuadrado
    	procesarRecorrido(delta);
    	
    	//procesa el efecto multicolor
    	//en caso de estar asignado
    	procesarMulticolor(delta);
    	
    	//procesa el efecto de 
    	//agrando y achicado
    	procesarAchicadoAgrandado(delta);

    	//procesa el efecto de achicado
    	//en caso de estar asignado
    	procesarAchicado(delta);
    	
    	//actualizamos la logica de 
    	//cada tipo de cuadrado
    	actualizar(gc, sbg, delta);
    	
    }
    
    public abstract void actualizar(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException;
    
    
    private void procesarAchicadoAgrandado(int delta) {
    	
    	//achicamos
    	if(efectoAchicadoAgrandadoEstado) { 
    		
        	//decrementamos el tiempo
        	tiempoActualAchicadoAgrandado -= delta;

        	//si llegamos a cero 
        	//finalizamos el achicado
        	if(tiempoActualAchicadoAgrandado <= 0) {
        		
        		//reseteamos el tiempo
        		tiempoActualAchicadoAgrandado = 0;
        		
        		//hacemos que comience 
        		//a agrandarse
        		efectoAchicadoAgrandadoEstado = false;
        		
        	}
    	}
    	else {

        	//incrementamos el tiempo
        	tiempoActualAchicadoAgrandado += delta;

        	//si llegamos al tiempo 
        	//maximo configurado
        	if(tiempoActualAchicadoAgrandado > tiempoConfiguradoAchicadoAgrandado) {
        		
        		//reseteamos el tiempo
        		tiempoActualAchicadoAgrandado = tiempoConfiguradoAchicadoAgrandado;
        		
        		//hacemos que comience 
        		//a achicarse
        		efectoAchicadoAgrandadoEstado = true;
        		
        	}
    	}

    	if(tiempoConfiguradoAchicadoAgrandado != 0) {
    		
        	//en base al tiempo reconfiguramos
        	//el ancho y el alto del cuadrado
    		width = (tiempoActualAchicadoAgrandado * ANCHO_ESTANDAR_FRENETICO) / tiempoConfiguradoAchicadoAgrandado;
    		height = (tiempoActualAchicadoAgrandado * ALTO_ESTANDAR_FRENETICO) / tiempoConfiguradoAchicadoAgrandado;
    		
    	}
    }
    
    private void procesarMulticolor(int delta) {
    	
		//si todavia queda tiempo multicolor
		if(tiempoRestanteMulticolor > 0) {
			
			//decrementamos el tiempo
			tiempoRestanteMulticolor -= delta;
			
			//hacemos la logica de multicolor
			//que es cambiar el color del cuadrado
			//muy rapidamente
			color.r = (float)Math.random();
			color.g = (float)Math.random();
			color.b = (float)Math.random();
			color.a = (float)Math.random();
			
			if(tiempoRestanteMulticolor <= 0) {
				
				//normalizamos el tiempo para 
				//que no quede menor que cero
				tiempoRestanteMulticolor = 0;
				
				//volvermos al color original
				//del cuadrado
				volverColorOriginal();
			}
		}
    }
    
    private void procesarAchicado(int delta) {
    	
		//si todavia queda tiempo achicado
		if(tiempoRestanteAchicado > 0) {
			
			//decrementamos el tiempo
			tiempoRestanteAchicado -= delta;
			
			//hacemos la logica de achicado
			//que es cambiar el width y el
			//height hasta que quedan en cero
			width = (tiempoRestanteAchicado * anchoOriginalAchicado) / tiempoConfiguradoAchicado;
			height = (tiempoRestanteAchicado * altoOriginalAchicado) / tiempoConfiguradoAchicado;
			
			if(tiempoRestanteAchicado <= 0) {
				
				//normalizamos el tiempo para 
				//que no quede menor que cero
				tiempoRestanteAchicado = 0;
				
				//removemos el cuadrado
				remover = true;
				
			}
		}
    }
    
	/**
	 * Inicia el efecto multicolor
	 */
	protected void iniciarEfectoMulticolor(int duracion) {
		
		/**
		 * 500 ms de multicolor multiloco
		 */
		tiempoRestanteMulticolor = duracion;
		
	}    
    
	/**
	 * Inicia el efecto de achicadocuadrado del jugador
	 */
	protected void iniciarEfectoAchicado(int duracion) {
		
		tiempoConfiguradoAchicado = duracion;
		tiempoRestanteAchicado = duracion;
		
		anchoOriginalAchicado = width;
		altoOriginalAchicado = height;
		
	}
	
	protected void inicarEfectoAchicadoAgrandado(int tiempoConfiguradoAchicadoAgrandado) {
		
		//configuramos variables
		this.tiempoConfiguradoAchicadoAgrandado = tiempoConfiguradoAchicadoAgrandado;
		this.tiempoActualAchicadoAgrandado = 0;

		//activamos el efecto
		achicadoAgrandadoHabilitado = true;

		//se setea en verdadero el estado
		//actual del efecto para que desde
		//el comienzo el cuadrado comience 
		//achicandose para despues agrandarse
		efectoAchicadoAgrandadoEstado = true;
		
	}
	

	/**
     * Regresa al color original del cuadrado.
     * Normalmente llamado luego de un efecto
     * multicolor para volver al color original
     * del mismo
     */
	protected abstract void volverColorOriginal();
    
    /**
     * Procesa el recorrido actual del cuadrado
     * @param delta
     */
    private void procesarRecorrido(int delta) {
    	
    	//solo si el cuadrado no es el
    	//del jugador
    	if(!(this instanceof CuadradoJugador)) {
    	
    		//recorrido del cuadrado
    		if(TIEMPO_MAXIMO_RECORRIDO > 0) {
    			
    			//recorrido actual en la curva bezier
    			float recorridoBezier = ((float)tiempoActualRecorrido * 1f) / TIEMPO_MAXIMO_RECORRIDO;
    			
    			/**
    			 * Obtenemos las coordenadas x,y 
    			 * para la posicion actual en la
    			 * curva de bezier
    			 */
    			Vector2f recorridoBezierVector2f = recorrido.pointAt(recorridoBezier);
    			
    			//configuramos en el cuadrado la nueva posicion
    			this.x = (int)recorridoBezierVector2f.x;
    			this.y = (int)recorridoBezierVector2f.y;
    			
    			//decrementamos el tiempo del recorrido
    			tiempoActualRecorrido += delta;
    			
    			if(tiempoActualRecorrido >= TIEMPO_MAXIMO_RECORRIDO) {
    				tiempoActualRecorrido = 0;
    				TIEMPO_MAXIMO_RECORRIDO = 0;
    				
    				//removemos el cuadrado
    				remover = true;
    				
    			}
    		}
    	}
    }
    
    /**
     * Procesa la posible colision entre
     * los cuadrados que se ven en pantalla
     */
    private void procesarColision(List<Cuadrado> cuadrados) {
    	
    	if(cuadrados != null) {
    		
        	//recorremos la lista de TODOS los cuadrados
        	for(Cuadrado cuadrado : cuadrados) {
        		
        		//omitimos el propio cuadrado
        		if(cuadrado != this) {
        			
            		if(this.colisiona(cuadrado)) {
            			
            			//agregamos la colision solo si no
            			//existe previamente en la misma
            			if(cuadradoColisionList.indexOf(cuadrado) == -1) {
            				
                			//referenciamos el cuadrado
                			//con el que colisionamos
                			cuadradoColisionList.add(cuadrado);
                			
                			//comienza la colision
                			comienzaColisionCuadradoEvent(cuadrado);
                			
            			}
            		}
        		}
        	}
    	}
    }
    
    /**
     * Llamado cuando se detecta una colision
     * entre esta instancia de cuadrado y 
     * otra diferente
     */
    protected abstract void comienzaColisionCuadradoEvent(Cuadrado cuadrado);
    
    /**
     * El estado de colision anterior al 
     * actual de este cuadrado
     */
    private boolean estadoAnteriorColision;
    private boolean estadoActualColision;
    
    /**
     * Chequea si colisiona el cuadrado
     * con el recibido por parametro
     * @param cuadrado
     * @return
     */
    public boolean colisiona(Cuadrado cuadrado) {
    	
		//actualizamos el helper de
		//colision con la posicion
		//actual del cuadrado
    	this.collisionHelper.setBounds(this.x, this.y, this.width, this.height);
		cuadrado.collisionHelper.setBounds(cuadrado.x, cuadrado.y, cuadrado.width, cuadrado.height);
    	
		if(collisionHelper.intersects(cuadrado.collisionHelper)) {
			estadoActualColision = true;
    	}
    	else {
    		estadoActualColision = false;
    	}
		
		//si no esta colisionando mas
		//pero en el ultimo estado si
		//lo estaba entonces termino 
		//la colision
		if(!estadoActualColision && estadoAnteriorColision) {
			
			//removemos el cuadrado
			//de la lista de colisionadores
			cuadradoColisionList.remove(cuadrado);
			
			//llamamos el callback de 
			//colision finalizada
			finalizaColisionCuadradoEvent(cuadrado);
			
		}
		
		//actualizo el estado 
		//de colision anterior
		estadoAnteriorColision = estadoActualColision;
		
		return estadoAnteriorColision;
    }
    
    public void crearRecorrido(DIFICULTAD dificultad) {
    	
    	int origenX = 0;
    	int origenY = 0;
    	int destinoX = 0;
    	int destinoY = 0;
    	
    	/**
    	 * Aleatoriamente elegimos un lado para salir
    	 */
    	byte origenLado = (byte)(Math.random() * 4f);
    	
    	switch (origenLado) {
		case 0:
			origenX = (int)(Math.random() * (float)Configuracion.SCREEN_WIDTH);
			origenY = -height;
			destinoX = origenX;
			destinoY = Configuracion.SCREEN_HEIGHT + height;
			break;
		case 1:
			origenX = Configuracion.SCREEN_WIDTH + width;
			origenY = (int)(Math.random() * (float)Configuracion.SCREEN_HEIGHT);
			destinoX = -width-width;
			destinoY = origenY;
			break;
		case 2:
			origenX = (int)(Math.random() * (float)Configuracion.SCREEN_WIDTH);
			origenY = Configuracion.SCREEN_HEIGHT + height;
			destinoX = origenX;
			destinoY = -height-height;
			break;
		case 3:
			origenX = -width;
			origenY = (int)(Math.random() * (float)Configuracion.SCREEN_HEIGHT);
			destinoX = Configuracion.SCREEN_WIDTH + width;
			destinoY = origenY;
			break;
		default:
			break;
		}
    	
    	switch (dificultad) {
		case FACIL:

			if(origenLado == 0 || origenLado == 2) {
				//division exacta de la altura en 3 partes
				int divisionesY = Configuracion.SCREEN_HEIGHT / 3;
				recorrido = new Curve(new Vector2f(origenX, origenY), new Vector2f(origenX, divisionesY), new Vector2f(origenX, divisionesY * 2), new Vector2f(destinoX, destinoY));
			}
			else if(origenLado == 1 || origenLado == 3) {
				
				//division exacta del ancho en 3 partes
				int divisionesX = Configuracion.SCREEN_WIDTH / 3;
				recorrido = new Curve(new Vector2f(origenX, origenY), new Vector2f(Configuracion.SCREEN_WIDTH - divisionesX, origenY), new Vector2f(Configuracion.SCREEN_WIDTH - (divisionesX * 2), origenY), new Vector2f(destinoX, destinoY));
			}
			
			TIEMPO_MAXIMO_RECORRIDO = 3000 + (int)(Math.random() * 1500f);
			
			break;
		case RELATIVAMENTE_FACIL:
			
			if(origenLado == 0 || origenLado == 2) {
				//division exacta de la altura en 3 partes
				int divisionesY = Configuracion.SCREEN_HEIGHT / 3;
				recorrido = new Curve(new Vector2f(origenX, origenY), new Vector2f(origenX, divisionesY), new Vector2f(origenX, divisionesY * 2), new Vector2f(destinoX, destinoY));
			}
			else if(origenLado == 1 || origenLado == 3) {
				
				//division exacta del ancho en 3 partes
				int divisionesX = Configuracion.SCREEN_WIDTH / 3;
				recorrido = new Curve(new Vector2f(origenX, origenY), new Vector2f(Configuracion.SCREEN_WIDTH - divisionesX, origenY), new Vector2f(Configuracion.SCREEN_WIDTH - (divisionesX * 2), origenY), new Vector2f(destinoX, destinoY));
			}
			
			TIEMPO_MAXIMO_RECORRIDO = 2000 + (int)(Math.random() * 1100f);			
			
			break;
		case MEDIANO:
			
			if(origenLado == 0 || origenLado == 2) {
				
				//division exacta de la altura en 3 partes
				int divisionesX = Configuracion.SCREEN_HEIGHT / 3;
				int divisionesY = Configuracion.SCREEN_HEIGHT / 3;
				
				//lado vuelque aleatorio
				boolean vuelque = (Math.random() * 100f) < 50f ? true : false;
				if(vuelque) {
					divisionesX = -divisionesX;
				}
				
				recorrido = new Curve(new Vector2f(origenX, origenY), new Vector2f(origenX + (divisionesX / 2), divisionesY), new Vector2f(origenX + (divisionesX / 2), divisionesY * 2), new Vector2f(destinoX, destinoY));
			}
			else if(origenLado == 1 || origenLado == 3) {
				
//				//division exacta de la altura en 3 partes
				int divisionesY = Configuracion.SCREEN_HEIGHT / 3;
				
				//lado vuelque aleatorio
				boolean vuelque = (Math.random() * 100f) < 50f ? true : false;
				if(vuelque) {
					divisionesY = -divisionesY;
				}
				
				//division exacta del ancho en 3 partes
				int divisionesX = Configuracion.SCREEN_WIDTH / 3;
				recorrido = new Curve(new Vector2f(origenX, origenY), new Vector2f(Configuracion.SCREEN_WIDTH - divisionesX, origenY + divisionesY), new Vector2f(Configuracion.SCREEN_WIDTH - (divisionesX * 2), origenY + divisionesY), new Vector2f(destinoX, destinoY));
			}

			
			TIEMPO_MAXIMO_RECORRIDO = 2000 + (int)(Math.random() * 1000f);			

			break;
		case DIFICIL:
			
			if(origenLado == 0 || origenLado == 2) {
				
				//division exacta de la altura en 3 partes
				int divisionesX = Configuracion.SCREEN_HEIGHT / 3;
				int divisionesY = Configuracion.SCREEN_HEIGHT / 3;
				
				//lado vuelque aleatorio
				boolean vuelque = (Math.random() * 100f) < 50f ? true : false;
				if(vuelque) {
					divisionesX = -divisionesX;
				}
				
				recorrido = new Curve(new Vector2f(origenX, origenY), new Vector2f(origenX + (divisionesX / 2), divisionesY), new Vector2f(origenX - (divisionesX / 2), divisionesY * 2), new Vector2f(destinoX, destinoY));
			}
			else if(origenLado == 1 || origenLado == 3) {
				
//				//division exacta de la altura en 3 partes
				int divisionesY = Configuracion.SCREEN_HEIGHT / 3;
				
				//lado vuelque aleatorio
				boolean vuelque = (Math.random() * 100f) < 50f ? true : false;
				if(vuelque) {
					divisionesY = -divisionesY;
				}
				
				//division exacta del ancho en 3 partes
				int divisionesX = Configuracion.SCREEN_WIDTH / 3;
				recorrido = new Curve(new Vector2f(origenX, origenY), new Vector2f(Configuracion.SCREEN_WIDTH - divisionesX, origenY + divisionesY), new Vector2f(Configuracion.SCREEN_WIDTH - (divisionesX * 2), origenY - divisionesY), new Vector2f(destinoX, destinoY));
			}

			
			TIEMPO_MAXIMO_RECORRIDO = 2000 + (int)(Math.random() * 500f);			
			
			break;
		case MUY_DIFICIL:
			
			if(origenLado == 0 || origenLado == 2) {
				
				//division exacta de la altura en 3 partes
				int divisionesX = Configuracion.SCREEN_HEIGHT / 3;
				int divisionesY = Configuracion.SCREEN_HEIGHT / 3;
				
				//lado vuelque aleatorio
				boolean vuelque = (Math.random() * 100f) < 50f ? true : false;
				if(vuelque) {
					divisionesX = -divisionesX;
				}
				
				recorrido = new Curve(new Vector2f(origenX, origenY), new Vector2f(origenX + (divisionesX * 2), divisionesY), new Vector2f(origenX - (divisionesX * 2), divisionesY * 2), new Vector2f(destinoX, destinoY));
			}
			else if(origenLado == 1 || origenLado == 3) {
				
//				//division exacta de la altura en 3 partes
				int divisionesY = Configuracion.SCREEN_HEIGHT / 3;
				
				//lado vuelque aleatorio
				boolean vuelque = (Math.random() * 100f) < 50f ? true : false;
				if(vuelque) {
					divisionesY = -divisionesY;
				}
				
				//division exacta del ancho en 3 partes
				int divisionesX = Configuracion.SCREEN_WIDTH / 3;
				recorrido = new Curve(new Vector2f(origenX, origenY), new Vector2f(Configuracion.SCREEN_WIDTH - divisionesX, origenY + (divisionesY * 2)), new Vector2f(Configuracion.SCREEN_WIDTH - (divisionesX * 2), origenY - (divisionesY * 2)), new Vector2f(destinoX, destinoY));
			}

			
			TIEMPO_MAXIMO_RECORRIDO = 1500 + (int)(Math.random() * 500f);
			
			break;
		case IMPOSIBLE:
			
			if(origenLado == 0 || origenLado == 2) {
				
				//division exacta de la altura en 3 partes
				int divisionesX = Configuracion.SCREEN_HEIGHT / 3;
				int divisionesY = Configuracion.SCREEN_HEIGHT / 3;
				
				//lado vuelque aleatorio
				boolean vuelque = (Math.random() * 100f) < 50f ? true : false;
				if(vuelque) {
					divisionesX = -divisionesX;
				}
				
				recorrido = new Curve(new Vector2f(origenX, origenY), new Vector2f(origenX + (divisionesX * 2), divisionesY), new Vector2f(origenX - (divisionesX), divisionesY * 2), new Vector2f(destinoX, destinoY));
			}
			else if(origenLado == 1 || origenLado == 3) {
				
				//division exacta de la altura en 3 partes
				int divisionesY = Configuracion.SCREEN_HEIGHT / 3;
				
				//lado vuelque aleatorio
				boolean vuelque = (Math.random() * 100f) < 50f ? true : false;
				if(vuelque) {
					divisionesY = -divisionesY;
				}
				
				//division exacta del ancho en 3 partes
				int divisionesX = Configuracion.SCREEN_WIDTH / 3;
				recorrido = new Curve(new Vector2f(origenX, origenY), new Vector2f(Configuracion.SCREEN_WIDTH - divisionesX, origenY + (divisionesY * 2)), new Vector2f(Configuracion.SCREEN_WIDTH - (divisionesX), origenY - (divisionesY * 2)), new Vector2f(destinoX, destinoY));
			}

			
			TIEMPO_MAXIMO_RECORRIDO = 1200 + (int)(Math.random() * 500f);
						
			break;
		default:
			break;
		}
    }
    
    public void iniciarFrenesi(int tiempo) {
    	
    	estadoFrenetico = true;
    	tiempoRestanteMulticolor = tiempo;
    }
}