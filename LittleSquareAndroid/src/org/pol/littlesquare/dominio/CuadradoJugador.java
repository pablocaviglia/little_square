package org.pol.littlesquare.dominio;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import org.pol.littlesquare.audio.SfxPlayer;
import org.pol.littlesquare.configuracion.Configuracion;
import org.pol.littlesquare.state.GameplayState;

public class CuadradoJugador extends Cuadrado {

	/**
	 * Color original del cuadrado del jugador
	 */
	private static final Color COLOR_ORIGINAL = Color.black;
	
	public CuadradoJugador() {
		
		//color del cuadrado
		color = new Color(COLOR_ORIGINAL.r, COLOR_ORIGINAL.g, COLOR_ORIGINAL.b);
		
		//configuramos el ancho y 
		//alto del cuadrado
		width = ANCHO_ESTANDAR_JUGADOR;
		height = ALTO_ESTANDAR_JUGADOR;

		//inicializa la posicion del jugador
		inicializarPosicionJugador();

	}
	
	public CuadradoJugador(int x, int y) {
		
		//color del cuadrado
		color = new Color(COLOR_ORIGINAL.r, COLOR_ORIGINAL.g, COLOR_ORIGINAL.b);
		
		//configuramos el ancho y 
		//alto del cuadrado
		width = ANCHO_ESTANDAR_JUGADOR;
		height = ALTO_ESTANDAR_JUGADOR;

		super.x = x;
		super.y = y;
	}
	
    /**
     * Vuelve a inicializar la posicion 
     * del jugador al medio de la pantalla 
     */
    private void inicializarPosicionJugador() {
    	
    	x = Configuracion.SCREEN_WIDTH / 2 - width / 2;
    	y = Configuracion.SCREEN_HEIGHT / 2 - height / 2;
    	
    }

	@Override
	public void renderizar(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		
		
	}
	
	@Override
	public void actualizar(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
		
	}
	
	/**
	 * Cambia la posicion del jugador a la indicada, y
	 * en caso de ser necesario la recalcula y cambia
	 * para que el cuadrado del jugador siempre quede
	 * dentro de los limites del area de juego
	 * 
	 * @param x
	 * @param y
	 */
	public void cambiarPosicionCuadradoJugador(int x, int y) {
		
		//pequeno ajuste para que la posicion
		//del dedo o el mouse haga que quede
		//el cuadrado en el medio
		x -= width / 2;
		y -= height / 2;
		
		if(x < (Configuracion.SCREEN_WIDTH - width)) {

			//para que no quede medio cuadrado
			//afuera en el lateral superior e
			//izquierdo
			if(x > 0) {
				this.x = x;	
			}
			else {
				this.x = 0;
			}
		}
		else {
			//nos fuimos de los limites
			this.x = Configuracion.SCREEN_WIDTH - width;
		}
		
		if(y < (Configuracion.SCREEN_HEIGHT - height)) {

			if(y > 0) {
				this.y = y;	
			}
			else {
				this.y = 0;
			}
		}
		else {
			//nos fuimos de los limites
			this.y = Configuracion.SCREEN_HEIGHT - height;
		}
	}
	
	@Override
	protected void comienzaColisionCuadradoEvent(Cuadrado cuadrado) {
		
		/**
		 * Si el cuadrado es amigo iniciamos
		 * el efecto multicolor en el cuadrado
		 * del jugador
		 */
		if(cuadrado instanceof CuadradoEnemigo && !cuadrado.estadoFrenetico) {
			
			/**
			 * Solo decrementamos la vida si
			 * no se encuentra en estado 
			 * multicolor
			 */
			if(tiempoRestanteMulticolor <= 0) {

				//reproducimos un yessss
				SfxPlayer.reproducir(SfxPlayer.OUCH);
				
				//iniciamos el efecto multicolor
				iniciarEfectoMulticolor(1000);

				//decrementamos vida
				GameplayState.decrementarVida();	
			}
		}
		else if(cuadrado instanceof CuadradoAmigo || (cuadrado instanceof CuadradoEnemigo && cuadrado.estadoFrenetico) ) {
			
			if(!cuadrado.finalizado) {
				
				//reproducimos un yessss
				SfxPlayer.reproducir(SfxPlayer.YES);
				
				//100 puntos
				GameplayState.score += 100;
				
			}
		}
		else if(cuadrado instanceof CuadradoVida) {
			
			if(!((CuadradoVida)cuadrado).finalizado) {
				
				//reproducimos un yessss
				SfxPlayer.reproducir(SfxPlayer.VIDA);
				
				//Generamos una nueva vida
				GameplayState.incrementarVida();
				
				//500 puntos
				GameplayState.score += 500;
				
			}
		}
		else if(cuadrado instanceof CuadradoFrenetico) {
			
			if(!((CuadradoFrenetico)cuadrado).finalizado) {
				
				//reproducimos un yessss
				SfxPlayer.reproducir(SfxPlayer.FRENETICO);
				
//				//Generamos un nuevo frenesi
				GameplayState.incrementarFrenesi();
				
				//250 puntos
				GameplayState.score += 250;
				
			}
		}
	}

	@Override
	protected void finalizaColisionCuadradoEvent(Cuadrado cuadrado) {
		volverColorOriginal();
	}
	
	protected void volverColorOriginal() {
		
		color.r = COLOR_ORIGINAL.r;
		color.g = COLOR_ORIGINAL.g;
		color.b = COLOR_ORIGINAL.b;
		color.a = COLOR_ORIGINAL.a;
	}
}