package org.pol.littlesquare.dominio;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class CuadradoFrenetico extends Cuadrado {
	
	/**
	 * Cuando el cuadrado es comido
	 * este estado es cambiado antes
	 * de destruirse el cuadrado
	 */
	public boolean finalizado;
	
    /**
     * Cantidad de cuadrados
     * amigos agarrados
     */
    public static int agarrados;
    
	public CuadradoFrenetico(int x, int y) {
		
		//color del cuadrado
		color = new Color(Color.orange);
		
		//configuramos el ancho y 
		//alto del cuadrado
		width = ANCHO_ESTANDAR_FRENETICO;
		height = ALTO_ESTANDAR_FRENETICO;
		
		//seteamos la posicion
		this.x = x;
		this.y = y;
		
		//iniciamos desde el inicio
		//el efecto de achicado y
		//agrandado que caracateriza
		//a este tipo de cuadrado
		inicarEfectoAchicadoAgrandado(500);

	}
	
	@Override
	public void renderizar(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		
		
	}
	
	@Override
	public void actualizar(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
		
	}
	
	@Override
	protected void comienzaColisionCuadradoEvent(Cuadrado cuadrado) {
		
		/**
		 * Si colisiona con el jugador
		 * entonces comenzamos la 
		 * animacion de desaparicion
		 * del cuadrado, y luego la
		 * reproduccion de un sonido
		 */
		if(cuadrado instanceof CuadradoJugador && !finalizado) {
			
			//comenzamos el efecto de achicado
			iniciarEfectoAchicado(600);
			
			//asi no entramos de nuevo 
			//a este evento
			finalizado = true;
			
			//incrementamos cantidad de
			//cuadrados amigos agarrados
			agarrados++;
			
		}
	}
	
	@Override
	protected void volverColorOriginal() {
		
	}
	
	protected void finalizaColisionCuadradoEvent(Cuadrado cuadrado) {
		
	}
}