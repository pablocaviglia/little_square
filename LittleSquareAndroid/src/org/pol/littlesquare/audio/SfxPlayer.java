package org.pol.littlesquare.audio;

import java.util.HashMap;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.pol.littlesquare.configuracion.Configuracion;

public class SfxPlayer {

	public final static byte OUCH = 0;
	public final static byte YES = 1;
	public final static byte VIDA = 2;
	public final static byte FRENETICO_MUSICA = 3;
	public final static byte FRENETICO = 4;
	
	public static HashMap<Byte, Sound> sonidos = new HashMap<Byte, Sound>();
	
	public static void loadSounds(byte[] soundIds) {
		
		for(byte id : soundIds) {
			Sound sound = obtenerSonidoPorId(id);
			sonidos.put(id, sound);
		}
	}

	public static void unloadSounds(byte[] soundIds) {
		
		for(byte id : soundIds) {
			Sound sound = sonidos.get(id);
			sound.stop();
			sound = null;
			sonidos.remove(id);
		}
	}

	public static void reproducir(byte id) {
		
		if(Configuracion.AUDIO_HABILITADO) {
			sonidos.get(id).play();	
		}
	}
	
	private static Sound obtenerSonidoPorId(byte id) {
		
		Sound sound = null;
		
		try {
			switch(id) {
			case OUCH:
				sound = new Sound("sfx/ouch.ogg");
				break;
			case YES:
				sound = new Sound("sfx/yes.ogg");
				break;
			case VIDA:
				sound = new Sound("sfx/vida.ogg");
				break;
			case FRENETICO_MUSICA:
				sound = new Sound("sfx/musica_frenetico.ogg");
				break;
			case FRENETICO:
				sound = new Sound("sfx/frenetico.ogg");
				break;
			}
		} catch (SlickException e) {
			throw new RuntimeException("Error cargando sfx id=" + id);
		}
		
		return sound;
	}
}