package org.pol.littlesquare.network;



public class NetworkFacade {

	private static String APP_CODE = "136463dhgsy26346gdjhgfjyhfk37543726642jbsjfgkjgbjfdgjsfghjsf465427865389068068ghsgd265136143769058o6vnxgcn4w436436314613fhfdhd";
	
	public static void saveHighscore(long userId, int score) {
		
		try {
			
			//primero autenticamos
			HttpUtil peticionAuth = new HttpUtil("http://poldevel.dyndns.org:8080/MadSquaresOAuthServer/AuthCodeServlet");
			peticionAuth.add("ac", APP_CODE);
			peticionAuth.add("ui", String.valueOf(userId * 124));
			
			//conectamos y obtenemos respuesta
			String respuestaAuth = peticionAuth.connect();
			if(respuestaAuth.startsWith("OK")) {

				//obtenemos el token generado
				//en el lado del servidor
				long token = Long.parseLong(respuestaAuth.split("_")[1]);
				
				//creamos helper de conexion
				HttpUtil peticion = new HttpUtil("http://poldevel.dyndns.org:8080/MadSquaresOAuthServer/SaveHighscoreServlet");
				
				//agregamos parametros
				peticion.add("u", String.valueOf(userId * 212));
				peticion.add("s", String.valueOf(score * 32));
				peticion.add("t", String.valueOf(token * 44));
				
				//conectamos y obtenemos respuesta
				String respuesta = peticion.connect();
				
				System.out.println(respuesta);
				
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}