package org.pol.littlesquare.android;

import org.newdawn.slick.AndroidLogSystem;
import org.newdawn.slick.AndroidResourceLocation;
import org.newdawn.slick.Game;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.opengl.InternalTextureLoader;
import org.newdawn.slick.util.ResourceLoader;
import org.pol.littlesquare.LittleSquareGame;
import org.pol.littlesquare.configuracion.Configuracion;
import org.pol.littlesquare.iface.IActivityRequestHandler;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public class LittleSquareAndroidAdMobActivity extends AndroidApplication implements IActivityRequestHandler {

	private AndroidAdMobGDXContainer container;
	
	private final int SHOW_ADS = 1;
	private final int HIDE_ADS = 0;
	
	private AdView adView;
	
	protected Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch(msg.what) {
				case SHOW_ADS: {
	                adView.setEnabled(true);
	                adView.setVisibility(android.view.View.VISIBLE);
	                adView.loadAd(new AdRequest());
					break;
				}
				case HIDE_ADS: {
					adView.setVisibility(View.GONE);
	                adView.setEnabled(false);
					break;
				}
			}
		}
	};
	
	@Override
	public void showAds(boolean show) {
		handler.sendEmptyMessage(show ? SHOW_ADS : HIDE_ADS);	
	}
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
    	super.onCreate(savedInstanceState);
    	
    	org.newdawn.slick.util.Log.setLogSystem(new AndroidLogSystem());
    	org.newdawn.slick.util.Log.error("Slick-AE 0");
    	
		InternalTextureLoader.get().clear();
		InternalTextureLoader.get().setHoldTextureData(true);
		
		ResourceLoader.removeAllResourceLocations();
		ResourceLoader.addResourceLocation(new AndroidResourceLocation(getAssets()));
	
		LittleSquareGame littleSquareGame = new LittleSquareGame();
		littleSquareGame.setHandler(this);
		
        start(littleSquareGame, Configuracion.SCREEN_WIDTH, Configuracion.SCREEN_HEIGHT);

    }

	
	/**
	 * Start the game. Should be called from onCreate.
	 * 
	 * @param game The game to be hosted
	 */
	public void start(Game game) {
		Display display = getWindowManager().getDefaultDisplay(); 
		int width = display.getWidth();
		int height = display.getHeight();
		
		start(game,width,height);
	}
	
	/**
	 * Start the game. Should be called from onCreate.
	 * 
	 * @param game The game to be hosted
	 * @param width The width to use for the game
	 * @param height The height to use for the game
	 */
	public void start(Game game, int width, int height) {
	    
		try {
		
			Display display = getWindowManager().getDefaultDisplay(); 
			int realWidth = display.getWidth();
			int realHeight = display.getHeight();
			
			Configuracion.SCREEN_WIDTH_REAL_ANDROID = realWidth;
			Configuracion.SCREEN_HEIGHT_REAL_ANDROID = realHeight;
			
			System.out.println("---------------------------> REAL WIDTH: " + realWidth);
			System.out.println("---------------------------> REAL HEIGHT: " + realHeight);
			
			container = new AndroidAdMobGDXContainer(game, width, height, realWidth, realHeight);
	    	container.setAndroidApplication(this);

	        // Create the layout
	        RelativeLayout layout = new RelativeLayout(this);

	        // Do the stuff that initialize() would do for you
	        requestWindowFeature(Window.FEATURE_NO_TITLE);
	        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);

	        // Create the libgdx View
	        View gameView = initializeForView(container, false);

	        // Create and setup the AdMob view
	        adView = new AdView(this, AdSize.BANNER, Configuracion.ADMOB_CODE); // Put in your secret key here
	        adView.loadAd(new AdRequest());

	        // Add the libgdx view
	        layout.addView(gameView);

	        // Add the AdMob view
	        RelativeLayout.LayoutParams adParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
	        //adParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
	        adParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

	        layout.addView(adView, adParams);

	        // Hook it all up
	        setContentView(layout);

	    } catch (SlickException e) {
	    	Log.e("SLICK", "Failed to create container", e);
	    }
	}
}
