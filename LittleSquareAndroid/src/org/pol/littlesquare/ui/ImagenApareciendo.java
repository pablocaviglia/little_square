package org.pol.littlesquare.ui;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

public class ImagenApareciendo extends UIComponent {

	//referencia a la imagen
	private Image imagen;
	
	//tiempo configurado para 
	//el efecto de aparicion
	private long tiempoAparicionTotal;
	private long tiempoAparicionActual;
	
	//flag que indica la 
	//aparicion de la imagen
	public boolean aparecer;
	
	//escalado durante la aparicion
	private float escaladoMaximo = 1f;
	private float escaladoActual;
	
	//calculo del tamano de la 
	//imagen final despues de 
	//escalada al maximo
	private int anchoImagenMaximo;
	private int anchoImagenActual;
	
	private int xOriginal;
	
	public ImagenApareciendo(int x, int y, Image imagen, long tiempoAparicion, float rotacion) {
		
		super(x, y);
		xOriginal = x;
		this.imagen = imagen;
		this.tiempoAparicionTotal = tiempoAparicion;
		
		//rotacion
		imagen.setRotation(rotacion);
		
		//calculamos el ancho
		//maximo posible de la
		//imagen
		anchoImagenMaximo = (int)(imagen.getWidth() * escaladoMaximo);
		
	}

	/**
	 * Diferencia de pixeles durante
	 * el escalado de la imagen. 
	 * Usado para ajustar la posicion
	 * en x para que se mantenga 
	 * centrada la aparicion
	 */
	private int diferenciaAnchoPx;
	
	@Override
	protected void actualizar(int delta) {
		
		//efecto de aparicion
		if(aparecer) {
			
			//restamos el tiempo
			tiempoAparicionActual += delta;

			//calculamos el ancho actual de la imagen
			int anchoImagenActualNuevo = (int)((tiempoAparicionActual * anchoImagenMaximo) / tiempoAparicionTotal);
			
			if(anchoImagenActual != anchoImagenActualNuevo) {
				diferenciaAnchoPx = anchoImagenActualNuevo - anchoImagenActual;
			}
			else {
				//para que no siga borrando
				diferenciaAnchoPx = 0;
			}
			
			//configuramos nuevo valor
			anchoImagenActual = anchoImagenActualNuevo;

			//restamos a la posicion en x
			//la diferencia de tamano por
			//el redimensionado
			x -= diferenciaAnchoPx;
			
			//normalizamos variables
			if(tiempoAparicionActual >= tiempoAparicionTotal) {
				aparecer = false;
				tiempoAparicionActual = 0;
				escaladoActual = 0f;
				anchoImagenActual = 0;
				x = xOriginal;
			}
		}
	}
	
	@Override
	protected void renderizar(Graphics g) {

		//efecto de aparicion
		if(aparecer) {
			
			//zoom de imagen
			escaladoActual = (tiempoAparicionActual * escaladoMaximo) / tiempoAparicionTotal;
			
			//dibujamos la imagen
			imagen.draw(x, y, escaladoActual);
			
		}
	}
}