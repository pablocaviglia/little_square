package org.pol.littlesquare.ui;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Graphics;
import org.pol.littlesquare.ui.event.EventListener;
import org.pol.littlesquare.ui.event.UIEvent;

public abstract class UIComponent {

	protected int x;
	protected int y;
	protected int width;
	protected int height;
	
	private List<EventListener> eventListeners;
	
	public UIComponent(int x, int y) {
		this(x,y,0,0);
	}
	
	public UIComponent(int x, int y, int width, int height) {
		
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		
		eventListeners = new ArrayList<EventListener>();
	}
	
	public void update(int delta) {
		
		actualizar(delta);

	}
	
	public void render(Graphics g) {
		
		renderizar(g);
	}
	
	/**
	 * Analiza si el cursor se encuentra
	 * sobre el componente y devuelve un
	 * flag booleano indicandolo
	 * @return
	 */
	public void touchDown(int eventX, int eventY) {
		
		System.out.println("------------> X: " + x + " WIDTH: " + width + " Y: " + y + " HEIGHT: " + height + " EX: " + eventX + " EY: " + eventY);
		
		if(eventX >= x && eventX <= (x + width) && eventY >= y && eventY <= (y + height)) {
			
			generarEvento(new UIEvent(this));
		}
	}

	
	protected abstract void actualizar(int delta);
	
	protected abstract void renderizar(Graphics g);
	
	public void addEventListener(EventListener listener) {
		eventListeners.add(listener);
	}
	
	public void generarEvento(UIEvent event) {
		for(EventListener eventListener : eventListeners) {
			eventListener.eventoGenerado(event);
		}
	}
}