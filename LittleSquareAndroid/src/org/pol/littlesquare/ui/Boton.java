package org.pol.littlesquare.ui;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.pol.littlesquare.LittleSquareGame;

public class Boton extends UIComponent {

	private Color colorPrimario = new Color(0.39f,0.58f,0.95f, 0.35f);
	
	private String texto;
	private int textoWidth;
	private int textoHeight;
	
	public Boton(int x, int y, int width, int height, String texto) {
		
		//constructor padre
		super(x, y, width, height);
		
		//referenciamos texto y
		//calculamos su ancho
		//y alto
		this.texto = texto;
		textoWidth = LittleSquareGame.angelCodeFont.getWidth(texto);
		textoHeight = LittleSquareGame.angelCodeFont.getHeight(texto);

	}
	
	@Override
	public void actualizar(int delta) {
		
	}

	@Override
	public void renderizar(Graphics g) {

		g.setColor(colorPrimario);
		g.fillRoundRect(x, y, width, height, 1);

		LittleSquareGame.angelCodeFont.drawString(x + ((width / 2) - (textoWidth / 2) ), y + ((height / 2) - (textoHeight / 2) ), texto);

	}
}